<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::any('/send' ,'App\Http\Controllers\Scrapy@sendData')->name('s_filter');

Auth::routes();

Route::get('/home', 'App\Http\Controllers\HomeController@index')->name('home');

Route::group(['middleware' => 'auth'], function () {
//	Route::resource('user', 'App\Http\Controllers\UserController', ['except' => ['show']]);
	Route::get('profile', ['as' => 'profile.edit', 'uses' => 'App\Http\Controllers\ProfileController@edit']);
	Route::put('profile', ['as' => 'profile.update', 'uses' => 'App\Http\Controllers\ProfileController@update']);
	Route::put('profile/password', ['as' => 'profile.password', 'uses' => 'App\Http\Controllers\ProfileController@password']);
});

Route::prefix('/dashboard')->middleware('auth')->group(function(){

    Route::any('/unique-data-report', 'App\Http\Controllers\HomeController@index_unique_data_report')->name('unique_data_report');
    Route::any('/unique-data-report/add', 'App\Http\Controllers\HomeController@unique_form_report')->name('a_form_unique_report');
    Route::any('/unique-data-report/edit/{id}', 'App\Http\Controllers\HomeController@unique_form_report')->name('u_form_unique_report');
    Route::any('/unique-data-report/save', 'App\Http\Controllers\HomeController@add_unique_data_report')->name('add_unique_data_report');
    Route::any('/unique-data-report/update/{id}', 'App\Http\Controllers\HomeController@update_unique_data_report')->name('update_unique_data_report');
    Route::any('/unique-data-report/delete/{id}', 'App\Http\Controllers\HomeController@delete_unique_data_report')->name('delete_unique_data_report');
    Route::post('/unique-data-report/map', 'App\Http\Controllers\HomeController@mapFormSubmit')->name('mapFormSubmit');


    Route::any('/sql-statements/result', 'App\Http\Controllers\HomeController@sql_run')->name('sql_run');
    Route::any('/sql-statements', 'App\Http\Controllers\HomeController@sql_index')->name('sql_index');

    Route::any('/visual-query', 'App\Http\Controllers\HomeController@visualQueryBuilder')->name('visualQ');
    Route::post('/json-result', 'App\Http\Controllers\HomeController@getTableColumns')->name('get_table');
    Route::post('/test-sql', 'App\Http\Controllers\HomeController@test_sql')->name('test_sql');
    Route::any('/', 'App\Http\Controllers\HomeController@index')->name('dashboard');

});

Route::prefix('/dashboard')->middleware(['role:Admin','auth'])->group(function () {

    Route::any('/users', 'App\Http\Controllers\UserController@index')->name('users');
    Route::any('/users/add', 'App\Http\Controllers\UserController@add')->name('user.add');
    Route::any('/users/store', 'App\Http\Controllers\UserController@store')->name('user.store');
    Route::any('/users/edit/{id}', 'App\Http\Controllers\UserController@edit')->name('user.edit');
    Route::post('users/{id}', 'App\Http\Controllers\UserController@update')->name('user.update');

    Route::get('/users/active/{id}', 'App\Http\Controllers\UserController@status_active')->name('user.active');
    Route::get('/users/inactive/{id}', 'App\Http\Controllers\UserController@status_inactive')->name('user.inactive');
    Route::get('/users/delete/{id}', 'App\Http\Controllers\UserController@status_delete')->name('user.delete');

    Route::any('/swap-lists', 'App\Http\Controllers\HomeController@show_swap_lists')->name('show_swap_lists');
    Route::any('/swap-lists/edit', 'App\Http\Controllers\HomeController@update_filter')->name('update_filter');
    Route::any('/swap-lists/delete/{id}', 'App\Http\Controllers\HomeController@delete_filter')->name('delete_filter');
    Route::any('/swap-lists/add/', 'App\Http\Controllers\HomeController@add_filter')->name('add_filter');

    Route::any('/scraped-data', 'App\Http\Controllers\HomeController@index_scraped_data')->name('scraped_data');
    Route::any('/scraped-data/add', 'App\Http\Controllers\HomeController@scraped_form')->name('a_form_scraped');
    Route::any('/scraped-data/edit/{id}', 'App\Http\Controllers\HomeController@scraped_form')->name('u_form_scraped');
    Route::any('/scraped-data/save', 'App\Http\Controllers\HomeController@add_scraped_data')->name('add_scraped_data');
    Route::any('/scraped-data/update/{id}', 'App\Http\Controllers\HomeController@update_scraped_data')->name('update_scraped_data');
    Route::any('/scraped-data/delete/{id}', 'App\Http\Controllers\HomeController@delete_scraped_data')->name('delete_scraped_data');

    Route::any('/scraped-data-rent', 'App\Http\Controllers\RentDataController@index')->name('scraped_data_rent');
    Route::any('/scraped-data-rent/add', 'App\Http\Controllers\RentDataController@form')->name('a_form_scraped_rent');
    Route::any('/scraped-data-rent/edit/{id}', 'App\Http\Controllers\RentDataController@form')->name('u_form_scraped_rent');
    Route::any('/scraped-data-rent/save', 'App\Http\Controllers\RentDataController@add')->name('add_scraped_data_rent');
    Route::any('/scraped-data-rent/update/{id}', 'App\Http\Controllers\RentDataController@update')->name('update_scraped_data_rent');
    Route::any('/scraped-data-rent/delete/{id}', 'App\Http\Controllers\RentDataController@delete')->name('delete_scraped_data_rent');



    Route::any('/unique-data', 'App\Http\Controllers\HomeController@index_unique_data')->name('unique_data');
    Route::any('/unique-data/add', 'App\Http\Controllers\HomeController@unique_form')->name('a_form_unique');
    Route::any('/unique-data/edit/{id}', 'App\Http\Controllers\HomeController@unique_form')->name('u_form_unique');
    Route::any('/unique-data/save', 'App\Http\Controllers\HomeController@add_unique_data')->name('add_unique_data');
    Route::any('/unique-data/update/{id}', 'App\Http\Controllers\HomeController@update_unique_data')->name('update_unique_data');
    Route::any('/unique-data/delete/{id}', 'App\Http\Controllers\HomeController@delete_unique_data')->name('delete_unique_data');



    Route::any('/logs', 'App\Http\Controllers\LogController@index')->name('log.index');

});
