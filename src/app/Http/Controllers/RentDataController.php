<?php

namespace App\Http\Controllers;

use App\Helper\CommonHelper;
use App\Models\Scraped_data_rent;
use Composer\Models\Command\SearchCommand;
use Illuminate\Http\Request;

use Grids;
use HTML;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Schema;
use Nayjest\Grids\Components\Base\RenderableRegistry;
use Nayjest\Grids\Components\ColumnHeadersRow;
use Nayjest\Grids\Components\ColumnsHider;
use Nayjest\Grids\Components\CsvExport;
use Nayjest\Grids\Components\ExcelExport;
use Nayjest\Grids\Components\Filters\DateRangePicker;
use Nayjest\Grids\Components\FiltersRow;
use Nayjest\Grids\Components\HtmlTag;
use Nayjest\Grids\Components\Laravel5\Pager;
use Nayjest\Grids\Components\OneCellRow;
use Nayjest\Grids\Components\RecordsPerPage;
use Nayjest\Grids\Components\RenderFunc;
use Nayjest\Grids\Components\ShowingRecords;
use Nayjest\Grids\Components\TFoot;
use Nayjest\Grids\Components\THead;
use Nayjest\Grids\Components\TotalsRow;
use Nayjest\Grids\DbalDataProvider;
use Nayjest\Grids\EloquentDataProvider;
use Nayjest\Grids\FieldConfig;
use Nayjest\Grids\FilterConfig;
use Nayjest\Grids\Grid;
use Nayjest\Grids\GridConfig;
use DB;

use Goutte;

class RentDataController extends Controller
{

    public function index()
    {

        $query = Scraped_data_rent::query();

        $grid = new Grid(
            (new GridConfig)
                ->setDataProvider(
                    new EloquentDataProvider($query)
                )
                ->setName('swapper')
                ->setPageSize(30)
                ->setColumns([
                    (new FieldConfig)
                        ->setName('AdId')
                        ->setLabel('Id')
                        ->setSortable(true)
                        ->addFilter(
                            (new FilterConfig)
                                ->setOperator(FilterConfig::OPERATOR_LIKE)
                        )
                    ,
                    (new FieldConfig)
                        ->setName('ExportTimeStamp')
                        ->setLabel('ExportTimeStamp')
                        ->setSortable(true)

                    ,
                    (new FieldConfig)
                        ->setName('AdUrl')
                        ->setLabel('Url')
                        ->setSortable(true)
                        ->addFilter(
                            (new FilterConfig)
                                ->setOperator(FilterConfig::OPERATOR_LIKE)
                        )
                    ,

                    (new FieldConfig)
                        ->setName('AdName')
                        ->setLabel('Name')
                        ->setSortable(true)
                        ->addFilter(
                            (new FilterConfig)
                                ->setOperator(FilterConfig::OPERATOR_LIKE)
                        )
                    ,
                    (new FieldConfig)
                        ->setName('AdInfo')
                        ->setLabel('Info')
                        ->setSortable(true)
                        ->addFilter(
                            (new FilterConfig)
                                ->setOperator(FilterConfig::OPERATOR_LIKE)
                        )
                    ,
                    (new FieldConfig)
                        ->setName('AdPrice')
                        ->setLabel('Price')
                        ->setSortable(true)
                        ->addFilter(
                            (new FilterConfig)
                                ->setOperator(FilterConfig::OPERATOR_LIKE)
                        )

                    ,

                    (new FieldConfig)
                        ->setName('AdCity')
                        ->setLabel('AdCity')
                        ->setSortable(true)
                        ->addFilter(
                            (new FilterConfig)
                                ->setOperator(FilterConfig::OPERATOR_LIKE)
                        )
                    ,

                    (new FieldConfig)
                        ->setName('AdPostCode')
                        ->setLabel('PostCode')
                        ->setSortable(true)
                        ->addFilter(
                            (new FilterConfig)
                                ->setOperator(FilterConfig::OPERATOR_LIKE)
                        )
                    ,
                    (new FieldConfig)
                        ->setName('AdDistrict')
                        ->setLabel('District')
                        ->setSortable(true)
                        ->addFilter(
                            (new FilterConfig)
                                ->setOperator(FilterConfig::OPERATOR_LIKE)
                        )


                    ,
                    (new FieldConfig)
                        ->setName('AdRooms')
                        ->setLabel('Rooms')
                        ->setSortable(true)
                        ->addFilter(
                            (new FilterConfig)
                                ->setOperator(FilterConfig::OPERATOR_LIKE)
                        )
                    ,
                    (new FieldConfig)
                        ->setName('AdSquaremeters')
                        ->setLabel('Squaremeters')
                        ->setSortable(true)
                        ->addFilter(
                            (new FilterConfig)
                                ->setOperator(FilterConfig::OPERATOR_LIKE)
                        )


                    ,
                    (new FieldConfig)
                        ->setName('AdPricePerSquaremeters')
                        ->setLabel('PricePerSquaremeters')
                        ->setSortable(true)
                        ->addFilter(
                            (new FilterConfig)
                                ->setOperator(FilterConfig::OPERATOR_LIKE)
                        )


                    ,
                    (new FieldConfig)
                        ->setName('FromUrl')
                        ->setLabel('FromUrl')
                        ->setSortable(true)
                        ->addFilter(
                            (new FilterConfig)
                                ->setOperator(FilterConfig::OPERATOR_LIKE)
                        )


                    ,
                    (new FieldConfig)
                        ->setName('AdRegionUrl')
                        ->setLabel('AdRegionUrl')
                        ->setSortable(true)
                        ->addFilter(
                            (new FilterConfig)
                                ->setOperator(FilterConfig::OPERATOR_LIKE)
                        )
                    ,

                    (new FieldConfig)
                        ->setName('AdSonstiges')
                        ->setLabel('Sonstiges')
                        ->setCallback(function ($val) {
                            if(strlen($val) > 70){
                                $small = substr($val, 0, 70);
                                return '<g class="short-text">' . $small .  '... <a class="see-more" href="javascript:void(0);">See More</a></g>
                                    <g class="full-text hide">' . $val .  '... <a class="see-less" href="javascript:void(0);">See Less</a></p>';
                            }else{
                                return $val;
                            }
                        })
                        ->setSortable(true)
                        ->addFilter(
                            (new FilterConfig)
                                ->setOperator(FilterConfig::OPERATOR_LIKE)
                        )
                    ,

                    (new FieldConfig)
                        ->setName('AdDate')
                        ->setLabel('Date')
                        ->setSortable(true)
                        ->addFilter(
                            (new FilterConfig)
                                ->setOperator(FilterConfig::OPERATOR_LIKE)
                        )
                    ,
                    (new FieldConfig)
                        ->setName('AdTime')
                        ->setLabel('Time')
                        ->setSortable(true)
                        ->addFilter(
                            (new FilterConfig)
                                ->setOperator(FilterConfig::OPERATOR_LIKE)
                        )
                    ,

                    (new FieldConfig)
                        ->setName('id')
                        ->setLabel('Action')
                        ->setCallback(function ($val) {
                            $edit = '<span class="fas fa-edit"></span>&nbsp;';
                            $del = '<span class="fas fa-trash"></span>&nbsp;';
                            return
                                '<small style="margin-right:10px;">'
                                . $edit
                                . HTML::link(route('u_form_scraped_rent', ['id' => $val]), "EDIT")
                                . '</small><br>'
                                . '<small style="margin-right:20px;">'
                                . $del
                                . HTML::link(route('delete_scraped_data_rent', ['id' => $val]), "DELETE")
                                . '</small>';

                        })
                    ,

                ])
                ->setComponents([
                    (new THead)
                        ->setComponents([
                            //(new HtmlTag)->setAttributes(['class' => 'float-right'])->addComponent(new Pager),
                            (new HtmlTag)
                                ->setAttributes(['class' => 'float-left pb-2'])
                                ->addComponent(new ShowingRecords)
                            ,
                            (new ColumnHeadersRow),
                            (new FiltersRow)
                                ->addComponents([
                                    (new RenderFunc(function () {
                                        return "<style>
                                                .daterangepicker td.available.active,
                                                .daterangepicker li.active,
                                                .daterangepicker li:hover {
                                                    color:black !important;
                                                    font-weight: bold;
                                                }
                                           </style>";
                                    }))
                                        ->setRenderSection('filters_row_column_ExportTimeStamp'),
                                    (new DateRangePicker)
                                        ->setName('ExportTimeStamp')
                                        ->setRenderSection('filters_row_column_ExportTimeStamp')
                                        ->setDefaultValue([date("Y-m-d", strtotime("-3 months")), date('Y-m-d')])
                                ]),
                            (new OneCellRow)
                                ->setRenderSection(RenderableRegistry::SECTION_END)
                                ->setComponents([
                                    new RecordsPerPage,
                                    // new ColumnsHider(),
                                    (new CsvExport)->setFileName('scrapedData' . date('Y-m-d'))->setRowsLimit
                                    (50000000),
                                    (new ExcelExport)->setFileName('scrapedData' . date('Y-m-d'))->setIgnoredColumns
                                    (['Action'])->setRowsLimit(50000000),
                                    (new HtmlTag)
                                        ->setContent('<span class="fas fa-refresh"></span> Filter')
                                        ->setTagName('button')
                                        ->setRenderSection(RenderableRegistry::SECTION_END)
                                        ->setAttributes([
                                            'class' => 'btn btn-success btn-sm'
                                        ])
                                ])
                        ])
                    ,
                    (new TFoot)
                        ->setComponents([

                            (new OneCellRow)
                                ->setComponents([
                                    new Pager,
                                    (new HtmlTag)
                                        ->setAttributes(['class' => 'pull-right'])
                                        ->addComponent(new ShowingRecords)
                                    ,
                                ])
                        ])
                    ,
                ])
        );

        $grid = $grid->render();
        return view('scraped_data_rent.index', compact('grid'));
    }

    public function form($id = '')
    {
        if ($id) {
            $edit = Scraped_data_rent::where('id', $id)->first();
            return view('scraped_data_rent.form', compact('edit'));
        }
        return view('scraped_data_rent.form');
    }

    public function add(Request $request)
    {
        Scraped_data_rent::create($request->all());
        return redirect()->route('scraped_data_rent')->with('message', 'Data Added!');
    }

    public function update(Scraped_data_rent $id, Request $request)
    {
        $id->update($request->all());
        return redirect()->route('scraped_data_rent')->with('message', 'Data Updated!');
    }

    public function delete(Scraped_data_rent $id)
    {
        $id->delete();
        return redirect()->back()->with('message', 'Data Deleted!!');
    }


}
