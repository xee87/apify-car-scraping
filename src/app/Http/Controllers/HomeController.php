<?php

namespace App\Http\Controllers;

use App\Exports\GridExport;
use App\Helper\CommonHelper;
use App\Models\Scraped_data;
use Composer\Command\SearchCommand;
use Illuminate\Http\Request;
use App\Models\Swap_list;
use App\Models\Unique_data;
use App\Models\Unique_data_report;

use Grids;
use HTML;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Schema;
use Nayjest\Grids\Components\Base\RenderableRegistry;
use Nayjest\Grids\Components\ColumnHeadersRow;
use Nayjest\Grids\Components\ColumnsHider;
use Nayjest\Grids\Components\CsvExport;
use Nayjest\Grids\Components\ExcelExport;
use Nayjest\Grids\Components\Filters\DateRangePicker;
use Nayjest\Grids\Components\FiltersRow;
use Nayjest\Grids\Components\HtmlTag;
use Nayjest\Grids\Components\Laravel5\Pager;
use Nayjest\Grids\Components\OneCellRow;
use Nayjest\Grids\Components\RecordsPerPage;
use Nayjest\Grids\Components\RenderFunc;
use Nayjest\Grids\Components\ShowingRecords;
use Nayjest\Grids\Components\TFoot;
use Nayjest\Grids\Components\THead;
use Nayjest\Grids\Components\TotalsRow;
use Nayjest\Grids\DbalDataProvider;
use Nayjest\Grids\EloquentDataProvider;
use Nayjest\Grids\FieldConfig;
use Nayjest\Grids\FilterConfig;
use Nayjest\Grids\Grid;
use Nayjest\Grids\GridConfig;
use DB;

use Goutte;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('layouts.app');
    }

    public function login()
    {
        return view('auth.login');
    }

    public function register()
    {
        return view('auth.register');
    }

    public function show_swap_lists()
    {
        $s_lists = Swap_list::all();


        $grid = new Grid(
            (new GridConfig)
                ->setDataProvider(
                    new EloquentDataProvider(Swap_list::query())
                )
                ->setName('swapper')
                ->setPageSize(5)
                ->setColumns([
                    (new FieldConfig)
                        ->setName('id')
                        ->setLabel('ID')
                        ->setSortable(true)
                        ->setSorting(Grid::SORT_ASC)
                    ,
                    (new FieldConfig)
                        ->setName('old_value')
                        ->setLabel('Old Value')
                        ->setCallback(function ($val) {
                            $GLOBALS['old_val'] = $val;
                            return $val;
                        })
                        ->setSortable(true)
                        ->addFilter(
                            (new FilterConfig)
                                ->setOperator(FilterConfig::OPERATOR_LIKE)
                        )
                    ,
                    (new FieldConfig)
                        ->setName('new_value')
                        ->setLabel('New Value')
                        ->setCallback(function ($val) {
                            $GLOBALS['new_val'] = $val;
                            return $val;
                        })
                        ->setSortable(true)
                        ->addFilter(
                            (new FilterConfig)
                                ->setOperator(FilterConfig::OPERATOR_LIKE)
                        ),


                    (new FieldConfig)
                        ->setName('id')
                        ->setLabel('Action')
                        ->setCallback(function ($val) {
                            $edit = '<span class="fas fa-edit"></span>&nbsp;';
                            $del = '<span class="fas fa-trash"></span>&nbsp;';
                            return
                                '<small style="margin-right:10px;">'
                                . $edit
                                . HTML::link('', "EDIT", 'data-id="' . $val . '" data-new="' . $GLOBALS['new_val'] . '" data-old="' . $GLOBALS['old_val'] . '" class="update-filter" data-toggle="modal" data-target="#modal-update"')
                                . '</small>'
                                . '<small>'
                                . $del
                                . HTML::link(route('delete_filter', ['id' => $val]), "DELETE")
                                . '</small>';

                        })
                    ,

                ])
                ->setComponents([
                    (new THead)
                        ->setComponents([
                            (new ColumnHeadersRow),
                            (new OneCellRow)
                                ->setRenderSection(RenderableRegistry::SECTION_END)
                                ->setComponents([
                                    new RecordsPerPage,
//                                    new ColumnsHider,
                                    (new CsvExport)
                                        ->setFileName('my_report' . date('Y-m-d'))
                                    ,
//                                    new ExcelExport(),
                                    (new HtmlTag)
                                        ->setContent('<span class="glyphicon glyphicon-refresh"></span> Filter')
                                        ->setTagName('button')
                                        ->setRenderSection(RenderableRegistry::SECTION_END)
                                        ->setAttributes([
                                            'class' => 'btn btn-success btn-sm'
                                        ])
                                ])
                        ])
                    ,
                    (new TFoot)
                        ->setComponents([

                            (new OneCellRow)
                                ->setComponents([
                                    new Pager,
                                    (new HtmlTag)
                                        ->setAttributes(['class' => 'pull-right'])
                                        ->addComponent(new ShowingRecords)
                                    ,
                                ])
                        ])
                    ,
                ])
        );


        $grid = $grid->render();
        // var_dump($grid); exit;
        return view('swap_list.index', compact('s_lists', 'grid'));
    }

    public function add_filter(Request $request)
    {

        // var_dump($request); exit;
        Swap_list::create($request->all());
        return redirect()->back()->with('status', 'Filter! Created');
    }

    public function update_filter(Request $request)
    {
        Swap_list::where('id', $request->id)->update(['old_value' => $request->old_value, 'new_value' => $request->new_value]);
        return redirect()->back()->with('status', 'Filter! Updated');
    }

    public function delete_filter(Swap_list $id)
    {
        $id->delete();
        return redirect()->back()->with('status', 'Filter! Deleted');
    }


    //Scraped Data

    public function index_scraped_data()
    {

        $query = Scraped_data::query();
       // var_dump($query); exit;
        //var_dump(DB::select('select AdPrice from scraped_data')); exit;
        $grid = new Grid(
            (new GridConfig)
                ->setDataProvider(
                    new EloquentDataProvider($query)
                )
                ->setName('swapper')
                ->setPageSize(30)
                ->setColumns([
                    (new FieldConfig)
                        ->setName('AdId')
                        ->setLabel('Id')
                        ->setSortable(true)
                        ->addFilter(
                            (new FilterConfig)
                                ->setOperator(FilterConfig::OPERATOR_LIKE)
                        )
                    ,
                    (new FieldConfig)
                        ->setName('ExportTimeStamp')
                        ->setLabel('Export TimeStamp')
                        ->setSortable(true)

                    ,
                    (new FieldConfig)
                        ->setName('AdUrl')
                        ->setLabel('Url')
                        ->setSortable(true)
                        ->addFilter(
                            (new FilterConfig)
                                ->setOperator(FilterConfig::OPERATOR_LIKE)
                        )
                    ,

                    (new FieldConfig)
                        ->setName('AdName')
                        ->setLabel('Name')
                        ->setSortable(true)
                        ->addFilter(
                            (new FilterConfig)
                                ->setOperator(FilterConfig::OPERATOR_LIKE)
                        )
                    ,
                    (new FieldConfig)
                        ->setName('AdLZ')
                        ->setLabel('LZ')
                        ->setSortable(true)
                        ->addFilter(
                            (new FilterConfig)
                                ->setOperator(FilterConfig::OPERATOR_LIKE)
                        )
                    ,
                    (new FieldConfig)
                        ->setName('AdPrice')
                        ->setLabel('Price')
                        ->setSortable(true)
                        ->addFilter(
                            (new FilterConfig)
                                ->setOperator(FilterConfig::OPERATOR_LIKE)
                        )

                    ,

                    (new FieldConfig)
                        ->setName('AdKm')
                        ->setLabel('KM')
                        ->setSortable(true)
                        ->addFilter(
                            (new FilterConfig)
                                ->setOperator(FilterConfig::OPERATOR_LIKE)
                        )
                    ,

                    (new FieldConfig)
                        ->setName('AdYear')
                        ->setLabel('Year')
                        ->setSortable(true)
                        ->addFilter(
                            (new FilterConfig)
                                ->setOperator(FilterConfig::OPERATOR_LIKE)
                        )
                    ,
                    (new FieldConfig)
                        ->setName('AdPublTime')
                        ->setLabel('Publish Time')
                        ->setSortable(true)
                        ->addFilter(
                            (new FilterConfig)
                                ->setOperator(FilterConfig::OPERATOR_LIKE)
                        )


                    ,
                    (new FieldConfig)
                        ->setName('FromUrl')
                        ->setLabel('From Url')
                        ->setSortable(true)
                        ->addFilter(
                            (new FilterConfig)
                                ->setOperator(FilterConfig::OPERATOR_LIKE)
                        )
                    ,


                    (new FieldConfig)
                        ->setName('id')
                        ->setLabel('Action')
                        ->setCallback(function ($val) {
                            $edit = '<span class="fas fa-edit"></span>&nbsp;';
                            $del = '<span class="fas fa-trash"></span>&nbsp;';
                            return
                                '<small style="margin-right:10px;">'
                                . $edit
                                . HTML::link(route('u_form_scraped', ['id' => $val]), "EDIT")
                                . '</small><br>'
                                . '<small style="margin-right:20px;">'
                                . $del
                                . HTML::link(route('delete_scraped_data', ['id' => $val]), "DELETE")
                                . '</small>';

                        })
                    ,

                ])
                ->setComponents([
                    (new THead)
                        ->setComponents([
                            //(new HtmlTag)->setAttributes(['class' => 'float-right'])->addComponent(new Pager),
                            (new HtmlTag)
                                ->setAttributes(['class' => 'float-left pb-2'])
                                ->addComponent(new ShowingRecords)
                            ,
                            (new ColumnHeadersRow),
                            (new FiltersRow)
                                ->addComponents([
                                    (new RenderFunc(function () {
                                        return "<style>
                                                .daterangepicker td.available.active,
                                                .daterangepicker li.active,
                                                .daterangepicker li:hover {
                                                    color:black !important;
                                                    font-weight: bold;
                                                }
                                           </style>";
                                    }))
                                        ->setRenderSection('filters_row_column_ExportTimeStamp'),
                                    (new DateRangePicker)
                                        ->setName('ExportTimeStamp')
                                        ->setRenderSection('filters_row_column_ExportTimeStamp')
                                        ->setDefaultValue([date("Y-m-d", strtotime("-3 months")), date('Y-m-d', strtotime('+1 day'))])
                                ]),
                            (new OneCellRow)
                                ->setRenderSection(RenderableRegistry::SECTION_END)
                                ->setComponents([
                                    new RecordsPerPage,
                                   // new ColumnsHider(),
                                    (new CsvExport)->setFileName('scrapedData' . date('Y-m-d'))->setRowsLimit
                                    (50000000),
                                    (new ExcelExport)->setFileName('scrapedData' . date('Y-m-d'))->setIgnoredColumns
                                    (['Action'])->setRowsLimit(50000000),
                                    (new HtmlTag)
                                        ->setContent('<span class="fas fa-refresh"></span> Filter')
                                        ->setTagName('button')
                                        ->setRenderSection(RenderableRegistry::SECTION_END)
                                        ->setAttributes([
                                            'class' => 'btn btn-success btn-sm'
                                        ])
                                ])
                        ])
                    ,
                    (new TFoot)
                        ->setComponents([

                            (new OneCellRow)
                                ->setComponents([
                                    new Pager,
                                    (new HtmlTag)
                                        ->setAttributes(['class' => 'pull-right'])
                                        ->addComponent(new ShowingRecords)
                                    ,
                                ])
                        ])
                    ,
                ])
        );

        $grid = $grid->render();
        return view('scraped_data.index', compact('grid'));
    }

    public function scraped_form($id = '')
    {
        if ($id) {
            $edit = Scraped_data::where('id', $id)->first();
            return view('scraped_data.form', compact('edit'));
        }
        return view('scraped_data.form');
    }

    public function add_scraped_data(Request $request)
    {
        Scraped_data::create($request->all());
        return redirect()->route('scraped_data')->with('status', 'Data Added!');
    }

    public function update_scraped_data(Scraped_data $id, Request $request)
    {
        $id->update($request->all());
        return redirect()->route('scraped_data')->with('status', 'Data Updated!');
    }

    public function delete_scraped_data(Scraped_data $id)
    {
        $id->delete();
        return redirect()->back()->with('status', 'Data Deleted!!');
    }

    //Unique Data

    public function index_unique_data()
    {

        $query = Unique_data::query();
        //var_dump($query); exit;
        // var_dump($query); exit;
        //var_dump(DB::select('select AdPrice from scraped_data')); exit;
        $grid = new Grid(
            (new GridConfig)
                ->setDataProvider(
                    new EloquentDataProvider($query)
                )
                ->setName('swapper')
                ->setPageSize(30)
                ->setColumns([
                    (new FieldConfig)
                        ->setName('scraped_id')
                        ->setLabel('ScrapedId')
                        ->setSortable(true)
                        ->addFilter(
                            (new FilterConfig)
                                ->setOperator(FilterConfig::OPERATOR_LIKE)
                        )
                    ,

                    (new FieldConfig)
                        ->setName('AdId')
                        ->setLabel('Id')
                        ->setSortable(true)
                        ->addFilter(
                            (new FilterConfig)
                                ->setOperator(FilterConfig::OPERATOR_LIKE)
                        )
                    ,
                    (new FieldConfig)
                        ->setName('ExportTimeStamp')
                        ->setLabel('ExportTimeStamp')
                        ->setSortable(true)

                    ,
                    (new FieldConfig)
                        ->setName('AdUrl')
                        ->setLabel('Url')
                        ->setSortable(true)
                        ->addFilter(
                            (new FilterConfig)
                                ->setOperator(FilterConfig::OPERATOR_LIKE)
                        )
                    ,

                    (new FieldConfig)
                        ->setName('AdName')
                        ->setLabel('Name')
                        ->setSortable(true)
                        ->addFilter(
                            (new FilterConfig)
                                ->setOperator(FilterConfig::OPERATOR_LIKE)
                        )
                    ,
                    (new FieldConfig)
                        ->setName('AdLZ')
                        ->setLabel('LZ')
                        ->setSortable(true)
                        ->addFilter(
                            (new FilterConfig)
                                ->setOperator(FilterConfig::OPERATOR_LIKE)
                        )
                    ,
                    (new FieldConfig)
                        ->setName('AdPrice')
                        ->setLabel('Price')
                        ->setSortable(true)
                        ->addFilter(
                            (new FilterConfig)
                                ->setOperator(FilterConfig::OPERATOR_LIKE)
                        )

                    ,

                    (new FieldConfig)
                        ->setName('AdKm')
                        ->setLabel('KM')
                        ->setSortable(true)
                        ->addFilter(
                            (new FilterConfig)
                                ->setOperator(FilterConfig::OPERATOR_LIKE)
                        )
                    ,

                    (new FieldConfig)
                        ->setName('AdYear')
                        ->setLabel('Year')
                        ->setSortable(true)
                        ->addFilter(
                            (new FilterConfig)
                                ->setOperator(FilterConfig::OPERATOR_LIKE)
                        )
                    ,
                    (new FieldConfig)
                        ->setName('AdPublTime')
                        ->setLabel('Publish Time')
                        ->setSortable(true)
                        ->addFilter(
                            (new FilterConfig)
                                ->setOperator(FilterConfig::OPERATOR_LIKE)
                        )


                    ,
                    (new FieldConfig)
                        ->setName('FromUrl')
                        ->setLabel('From Url')
                        ->setSortable(true)
                        ->addFilter(
                            (new FilterConfig)
                                ->setOperator(FilterConfig::OPERATOR_LIKE)
                        )
                    ,

                    (new FieldConfig)
                        ->setName('id')
                        ->setLabel('Action')
                        ->setCallback(function ($val) {
                            $edit = '<span class="fas fa-edit"></span>&nbsp;';
                            $del = '<span class="fas fa-trash"></span>&nbsp;';
                            return
                                '<small style="margin-right:10px;">'
                                . $edit
                                . HTML::link(route('u_form_unique', ['id' => $val]), "EDIT")
                                . '</small><br>'
                                . '<small style="margin-right:20px;">'
                                . $del
                                . HTML::link(route('delete_unique_data', ['id' => $val]), "DELETE")
                                . '</small>';

                        })
                    ,

                ])
                ->setComponents([
                    (new THead)
                        ->setComponents([
                        //    (new HtmlTag)->setAttributes(['class' => 'float-right'])->addComponent(new Pager),
                            (new HtmlTag)
                                ->setAttributes(['class' => 'float-left pb-2'])
                                ->addComponent(new ShowingRecords)
                            ,
                            (new ColumnHeadersRow),
                            (new FiltersRow)
                                ->addComponents([
                                    (new RenderFunc(function () {
                                        return "<style>
                                                .daterangepicker td.available.active,
                                                .daterangepicker li.active,
                                                .daterangepicker li:hover {
                                                    color:black !important;
                                                    font-weight: bold;
                                                }
                                           </style>";
                                    }))
                                        ->setRenderSection('filters_row_column_ExportTimeStamp'),
                                    (new DateRangePicker)
                                        ->setName('ExportTimeStamp')
                                        ->setRenderSection('filters_row_column_ExportTimeStamp')
                                        ->setDefaultValue([date("Y-m-d", strtotime("-12 months")), date('Y-m-d', strtotime('+1 day'))])
                                ]),
                            (new OneCellRow)
                                ->setRenderSection(RenderableRegistry::SECTION_END)
                                ->setComponents([
                                    new RecordsPerPage,
                                //    new ColumnsHider(),
                                    (new CsvExport)->setFileName('uniqueData' . date('Y-m-d'))->setRowsLimit
                                    (50000000),
                                    (new ExcelExport)->setFileName('uniqueData' . date('Y-m-d'))->setIgnoredColumns(['Action'])->setRowsLimit(50000000),
                                    (new HtmlTag)
                                        ->setContent('<span class="fas fa-refresh"></span> Filter')
                                        ->setTagName('button')
                                        ->setRenderSection(RenderableRegistry::SECTION_END)
                                        ->setAttributes([
                                            'class' => 'btn btn-success btn-sm'
                                        ])
                                ])
                        ])
                    ,
                    (new TFoot)
                        ->setComponents([

                            (new OneCellRow)
                                ->setComponents([
                                    new Pager,
                                    (new HtmlTag)
                                        ->setAttributes(['class' => 'pull-right'])
                                        ->addComponent(new ShowingRecords)
                                    ,
                                ])
                        ])
                    ,
                ])
        );

        $grid = $grid->render();
        return view('unique_data.index', compact('grid'));
    }

    public function unique_form($id = '')
    {
        if ($id) {
            $edit = Unique_data::where('id', $id)->first();
            return view('unique_data.form', compact('edit'));
        }
        return view('unique_data.form');
    }

    public function add_unique_data(Request $request)
    {
        Unique_data::create($request->all());
        return redirect()->route('unique_data')->with('status', 'Data Added!');
    }

    public function update_unique_data(Unique_data $id, Request $request)
    {
        $id->update($request->all());
        return redirect()->route('unique_data')->with('status', 'Data Updated!');
    }

    public function delete_unique_data(Unique_data $id)
    {
        $id->delete();
        return redirect()->back()->with('status', 'Data Deleted!!');
    }

    //Unique Data Report


    public function ajaxPostCodeCall(Request $request)
    {
        $dataSet = DB::table('unique_data_report')->whereIn('id', $request->data)->get();

        $outPut = $postCodesArr = [];

        if(count($dataSet) > 0){
            foreach($dataSet as $k => $singleRow){

                if(!in_array($singleRow->AdPostCode, $postCodesArr)){
                    $outPut[$singleRow->AdPostCode][] = $singleRow;
                    $postCodesArr[] = $singleRow->AdPostCode;
                }else{
                    $allKeys = array_keys($outPut);
                    foreach($allKeys as $key => $singleKey){
                        if($singleRow->AdPostCode == $singleKey){
                            $outPut[$singleKey][] = $singleRow;
                            break;
                        }
                    }
                }
            }

            //var_dump($outPut);
        }


        return response()->json($outPut); exit;
    }

    public function mapFormSubmit(Request $request){

        $ids = explode(',', $request->data_id);
        $dataSet = DB::table('unique_data_report')->whereIn('id', $ids)->get();
        $outPut = $postCodesArr = [];

        if(count($dataSet) > 0){
            foreach($dataSet as $k => $singleRow){
                if(!in_array($singleRow->AdPostCode, $postCodesArr)){
                    $outPut[$singleRow->AdPostCode][] = $singleRow;
                    $postCodesArr[] = $singleRow->AdPostCode;
                }else{
                    $allKeys = array_keys($outPut);
                    foreach($allKeys as $key => $singleKey){
                        if($singleRow->AdPostCode == $singleKey){
                            $outPut[$singleKey][] = $singleRow;
                            break;
                        }
                    }
                }
            }
        }

        return view('unique_data_report.map', compact('outPut'));

    }

    public function index_unique_data_report()
    {

        $query = Unique_data_report::query();
        $allowedPostcodes = CommonHelper::getAllowedPostCodes();
        if(!CommonHelper::isAdmin() && is_array($allowedPostcodes)){
            $query->where(function($q) use ($allowedPostcodes){
                foreach($allowedPostcodes as $postcode){
                    $q->orWhere('AdPostCode', 'LIKE', $postcode.'%');
                }
            });
        }

        $args = CommonHelper::isAdmin() ? true : false;

        $grid = new Grid(
            (new GridConfig)
                ->setDataProvider(
                    new EloquentDataProvider($query)
                )
                ->setName('swapper')
                ->setPageSize(30)
                ->setColumns([

//                    (new FieldConfig)
//                        ->setName('id')
//                        ->setLabel('Checks')
//                        ->setCallback(function ($val) {
//
//                            $row = new FieldConfig();
//
//                            return
//                                '<input type="checkbox" class="big-checkbox" value="' . $val . '" name="selectedCheck[]"/>';
//
//                        })
//                    ,

                    (new FieldConfig)
                        ->setName('unique_data_id')
                        ->setLabel('UniqueDataId')
                        ->setSortable(true)
                        ->addFilter(
                            (new FilterConfig)
                                ->setOperator(FilterConfig::OPERATOR_LIKE)
                        )
                    ,

                    (new FieldConfig)
                        ->setName('AdId')
                        ->setLabel('Id')
                        ->setSortable(true)
                        ->addFilter(
                            (new FilterConfig)
                                ->setOperator(FilterConfig::OPERATOR_LIKE)
                        )
                    ,
                    (new FieldConfig)
                        ->setName('ExportTimeStamp')
                        ->setLabel('ExportTimeStamp')
                        ->setSortable(true)

                    ,
                    (new FieldConfig)
                        ->setName('AdUrl')
                        ->setLabel('Url')
                        ->setSortable(true)
                        ->addFilter(
                            (new FilterConfig)
                                ->setOperator(FilterConfig::OPERATOR_LIKE)
                        )
                    ,

                    (new FieldConfig)
                        ->setName('AdName')
                        ->setLabel('Name')
                        ->setSortable(true)
                        ->addFilter(
                            (new FilterConfig)
                                ->setOperator(FilterConfig::OPERATOR_LIKE)
                        )
                    ,
                    (new FieldConfig)
                        ->setName('AdLZ')
                        ->setLabel('LZ')
                        ->setSortable(true)
                        ->addFilter(
                            (new FilterConfig)
                                ->setOperator(FilterConfig::OPERATOR_LIKE)
                        )
                    ,
                    (new FieldConfig)
                        ->setName('AdPrice')
                        ->setLabel('Price')
                        ->setSortable(true)
                        ->addFilter(
                            (new FilterConfig)
                                ->setOperator(FilterConfig::OPERATOR_LIKE)
                        )

                    ,

                    (new FieldConfig)
                        ->setName('AdKm')
                        ->setLabel('KM')
                        ->setSortable(true)
                        ->addFilter(
                            (new FilterConfig)
                                ->setOperator(FilterConfig::OPERATOR_LIKE)
                        )
                    ,

                    (new FieldConfig)
                        ->setName('AdYear')
                        ->setLabel('Year')
                        ->setSortable(true)
                        ->addFilter(
                            (new FilterConfig)
                                ->setOperator(FilterConfig::OPERATOR_LIKE)
                        )
                    ,
                    (new FieldConfig)
                        ->setName('AdPublTime')
                        ->setLabel('Publish Time')
                        ->setSortable(true)
                        ->addFilter(
                            (new FilterConfig)
                                ->setOperator(FilterConfig::OPERATOR_LIKE)
                        )


                    ,
                    (new FieldConfig)
                        ->setName('FromUrl')
                        ->setLabel('From Url')
                        ->setSortable(true)
                        ->addFilter(
                            (new FilterConfig)
                                ->setOperator(FilterConfig::OPERATOR_LIKE)
                        )
                    ,

                    (new FieldConfig)
                        ->setName('id')
                        ->setLabel('Action')
                        ->setCallback(function ($val) {
                            $edit = '<span class="fas fa-edit"></span>&nbsp;';
                            $del = '<span class="fas fa-trash"></span>&nbsp;';
                            return
                                '<small style="margin-right:10px;">'
                                . $edit
                                . HTML::link(route('u_form_unique_report', ['id' => $val]), "EDIT")
                                . '</small><br>'
                                . '<small style="margin-right:20px;">'
                                . $del
                                . HTML::link(route('delete_unique_data_report', ['id' => $val]), "DELETE")
                                . '</small>';

                        })
                    ,

                ])
                ->setComponents([
                    (new THead)
                        ->setComponents([
                            //    (new HtmlTag)->setAttributes(['class' => 'float-right'])->addComponent(new Pager),
                            (new HtmlTag)
                                ->setAttributes(['class' => 'float-left pb-2'])
                                ->addComponent(new ShowingRecords)
                            ,
                            (new ColumnHeadersRow),
                            (new FiltersRow)
                                ->addComponents([
                                    (new RenderFunc(function () {
                                        return "<style>
                                                .daterangepicker td.available.active,
                                                .daterangepicker li.active,
                                                .daterangepicker li:hover {
                                                    color:black !important;
                                                    font-weight: bold;
                                                }
                                           </style>";
                                    }))
                                        ->setRenderSection('filters_row_column_ExportTimeStamp'),
                                    (new DateRangePicker)
                                        ->setName('ExportTimeStamp')
                                        ->setRenderSection('filters_row_column_ExportTimeStamp')
                                        ->setDefaultValue([date("Y-m-d", strtotime("-3 months")), date('Y-m-d', strtotime('+1 day'))])


                                ]),

                            (new OneCellRow)
                                ->setRenderSection(RenderableRegistry::SECTION_END)
                                ->setComponents([
                                    new RecordsPerPage,
                                    //    new ColumnsHider(),
                                    (new CsvExport)->setFileName('uniqueDataReport' . date('Y-m-d'))->setRowsLimit
                                    (50000000),
                                    (new ExcelExport)->setFileName('uniqueDataReport' . date('Y-m-d'))->setIgnoredColumns(['Action'])->setRowsLimit(50000000),
                                    (new HtmlTag)
                                        ->setContent('<span class="fas fa-refresh"></span> Filter')
                                        ->setTagName('button')
                                        ->setRenderSection(RenderableRegistry::SECTION_END)
                                        ->setAttributes([
                                            'class' => 'btn btn-success btn-sm'
                                        ])
                                ])
                        ])
                    ,
                    (new TFoot)
                        ->setComponents([

                            (new OneCellRow)
                                ->setComponents([
                                    new Pager,
                                    (new HtmlTag)
                                        ->setAttributes(['class' => 'pull-right'])
                                        ->addComponent(new ShowingRecords)
                                    ,
                                ])
                        ])
                    ,
                ])
        );

        $grid = $grid->render();
        return view('unique_data_report.index', compact('grid'));
    }

    public function unique_form_report($id = '')
    {
        if ($id) {
            $edit = Unique_data_report::where('id', $id)->first();
            return view('unique_data_report.form', compact('edit'));
        }
        return view('unique_data_report.form');
    }

    public function add_unique_data_report(Request $request)
    {
        Unique_data_report::create($request->all());
        return redirect()->route('unique_data_report')->with('status', 'Data Added!');
    }

    public function update_unique_data_report(Unique_data_report $id, Request $request)
    {
        $id->update($request->all());
        return redirect()->route('unique_data_report')->with('status', 'Data Updated!');
    }

    public function delete_unique_data_report(Unique_data_report $id)
    {
        $id->delete();
        return redirect()->back()->with('status', 'Data Deleted!!');
    }


    public function downloadExcel(Request $request){
        $export = new ExcelExport();

        var_dump($export->getData());
    }



    //SQL STATEMENTS


    public function sql_index(){
        return view('sql_table.index');
    }

    public function sql_run(Request $request){

        $sql_statement = htmlspecialchars($request->sql_statement, ENT_QUOTES);
        if(stripos($sql_statement, 'limit') == false) $sql_statement .= ' limit 500';
//       var_dump($sql_statement); exit;
        $results = '';
        $error = '';

        try {
            $results = DB::select(DB::raw($sql_statement));
        } catch(\Illuminate\Database\QueryException $ex){
            $error = $ex->getMessage();
        }

        return view('sql_table.index', compact('results', 'sql_statement', 'error'));
    }

    public function test_sql(Request $request){

        $sqlObj = $request->sql_obj;

        $params = (!empty($sqlObj['params']))? $sqlObj['params'] : [];

        $where = $sqlObj && strlen($sqlObj['sql']) > 0 ? " where " . $sqlObj['sql'] : '';

        $sorting = (!empty($request->sorting))? ' order by ' . $request->sorting : '';

        $limit = (!empty($request->limit))? ' limit ' . $request->limit : '';

        $group_by = (!empty($request->group_by))? ' group by ' . $request->group_by : '';

        $fields = $request->fields;

        $allowedPostcodes = CommonHelper::getAllowedPostCodes();
        if(!CommonHelper::isAdmin() && is_array($allowedPostcodes)){
            foreach($allowedPostcodes as $postcode){
                if($where){
                    $where .= " or AdPostCode LIKE '".$postcode."%'";
                }else{
                    $where = " where AdPostCode LIKE '".$postcode."%'";
                }
            }
        }


        if(!empty($request->agg_func)){
            $fields = $fields . ', ' . $request->agg_func;
        }

        $sql = "SELECT " . $fields . " from " . $request->table . $where . $group_by . $sorting . $limit;


       // var_dump($sql); exit;


        $error = '';
        $results = '';
        try {
            $results = DB::select(DB::raw($sql),$params);
        } catch(\Illuminate\Database\QueryException $ex){
            $error = $ex->getMessage();
            var_dump($error); exit;
        }

        return response()->json($results);

    }

    public function getTableColumns(Request $request)
    {

        $schema = Schema::getColumnListing($request->table);
        $filtersData = [];

        foreach($schema as $k => $field){

            if($field == 'ExportTimeStamp'){
                array_push($filtersData, array(
                    'id' => $field,
                    'label' => $field,
                    'type' => 'date',
                    'validation' => array('format' => 'Y-m-d h:m:s'),
                    'plugin' => 'datetimepicker',
                    'plugin_config' => array(
                        'format' => 'Y-m-d h:m:s',
                        'todayBtn' => 'linked',
                        'todayHighlight' => true,
                        'autoclose' => true,
                    )
                ));
            }else{
                array_push($filtersData, array(
                    'id' => $field,
                    'lable' => $field,
                    'type' => 'string'
                ));
            }

        }

        return response()->json(array('schema' => $schema, 'filtersData' => $filtersData));

    }

    public function visualQueryBuilder(){
        return view('sql_table.visualQuery');
    }

    public function debug(){
        $dataSet = DB::table('unique_data_report')->whereIn('id', ['42971','42972', '42973', '42974', '42975'])->get();

        $outPut = $postCodesArr = [];

        if(count($dataSet) > 0){
            foreach($dataSet as $k => $singleRow){

                if(!in_array($singleRow->AdPostCode, $postCodesArr)){
                    $outPut[$singleRow->AdPostCode][] = $singleRow;
                    $postCodesArr[] = $singleRow->AdPostCode;
                }else{
                    $allKeys = array_keys($outPut);
                    foreach($allKeys as $key => $singleKey){
                        if($singleRow->AdPostCode == $singleKey){
                            $outPut[$singleKey][] = $singleRow;
                            break;
                        }
                    }
                }
            }

            var_dump($outPut);
        }
        exit;

    }

///
}
