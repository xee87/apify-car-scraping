<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Faker\Provider\DateTime;
use App\Models\Scraped_data;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Mockery\Exception;
use Visitor;
use Carbon\Carbon;
use App\Models\Swap_list;
use Illuminate\Support\Facades\Log;

class Scrapy extends Controller
{


    protected $filters;

    public function __construct()
    {
        $this->filters = Swap_list::all();
    }

    public function sendData(Request $request)
    {

        try{
            $json = file_get_contents('php://input');
            $action = json_decode($json, true);
            $taskId = $action['eventData']['actorTaskId'];
            //  $runId = $action['resource']['actorRunId'];

            $ch1 = curl_init();
            curl_setopt($ch1, CURLOPT_FOLLOWLOCATION, TRUE);
            curl_setopt($ch1, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch1, CURLOPT_URL, 'https://api.apify.com/v2/actor-tasks/' . $taskId . '/runs/last/dataset/items?token=RxxJnwxk3GkRTcQgsP3DKGHk8&ui=1');
            curl_setopt($ch1, CURLOPT_HEADER, 0);
            curl_setopt($ch1, CURLOPT_CUSTOMREQUEST, "GET");
            curl_setopt($ch1, CURLOPT_RETURNTRANSFER, true);

            $result_json1 = curl_exec($ch1);

            curl_close($ch1);
            $scrap_data = json_decode($result_json1);

            if (!empty($scrap_data)) {

                foreach ($scrap_data as $data) {

                    if(!isset($data->AdID)) continue;

                    $now = new \DateTime('now', new \DateTimeZone( 'Europe/Berlin' ));
                    $now = $now->format('Y-m-d H:i:s.u');
                    //if (!Scraped_data::where('AdUrl', $data->f01_AdUrl)->exists()) {

                    $datatobeSaved = array(

                        'AdId' => $data->AdID,
                        'AdName' => $this->purifyData($data->f00_AdName),
                        'AdUrl' => $data->f01_AdUrl,
                        'AdPrice' => $this->convertInt($data->f02_AdPrice),
                        'AdKm' => $data->f03_AdKm,
                        'AdLZ' => $data->f04_AdLZ,
                        'AdYear' => $data->f05_AdYear,
                        'AdPublTime' => $data->f06_AdTime,
                        'FromUrl' => $data->f07_FromURL,
                        'ExportTimeStamp' => $now,
                        'BatchId' => $taskId

                        //'ExportDate' => $data->f13_ExportDate,
                        //'ExportTime' => $data->f14_ExportTime,
                        //'ExportTimeStamp' => $data->f15_ExportTimeStamp,
                    );

                    Scraped_data::create($datatobeSaved);

                    //}

                }
                http_response_code();

            } else {
                //echo 'NO DATA';
            }
        }catch (Exception $exception){
            Log::debug($exception->getMessage());
            Log::debug($exception->getLine());
        }


		exit;
    }

    public function convertInt($str){

        if($str == 'NaN' || $str == 'null')
            return null;

        if(strpos($str, ',') !== false )
            return (int)explode(',', $str)[0];

        return (int)$str;

    }


    public function purifyData($str){

        $cleanStr = preg_replace('/\s\s+/', ' ', preg_replace('/[^(\x20-\x7F)]*/','', $str));
        $cleanStr = iconv(mb_detect_encoding($cleanStr, mb_detect_order(), true), "UTF-8", $cleanStr);

        foreach($this->filters as $f){

            if($f->old_value == $cleanStr){
                $cleanStr = $f->new_value;
            }

        }

        return $cleanStr;
    }

    public function debug(){


    }

}
