<?php

namespace App\Http\Controllers;

use App\Helper\CommonHelper;
use App\Models\Scraped_data;
use Composer\Command\SearchCommand;
use Illuminate\Http\Request;
use App\Models\Logs;


use Grids;
use HTML;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Schema;
use Nayjest\Grids\Components\Base\RenderableRegistry;
use Nayjest\Grids\Components\ColumnHeadersRow;
use Nayjest\Grids\Components\ColumnsHider;
use Nayjest\Grids\Components\CsvExport;
use Nayjest\Grids\Components\ExcelExport;
use Nayjest\Grids\Components\Filters\DateRangePicker;
use Nayjest\Grids\Components\FiltersRow;
use Nayjest\Grids\Components\HtmlTag;
use Nayjest\Grids\Components\Laravel5\Pager;
use Nayjest\Grids\Components\OneCellRow;
use Nayjest\Grids\Components\RecordsPerPage;
use Nayjest\Grids\Components\RenderFunc;
use Nayjest\Grids\Components\ShowingRecords;
use Nayjest\Grids\Components\TFoot;
use Nayjest\Grids\Components\THead;
use Nayjest\Grids\Components\TotalsRow;
use Nayjest\Grids\DbalDataProvider;
use Nayjest\Grids\EloquentDataProvider;
use Nayjest\Grids\FieldConfig;
use Nayjest\Grids\FilterConfig;
use Nayjest\Grids\Grid;
use Nayjest\Grids\GridConfig;
use DB;

use Goutte;

class LogController extends Controller
{

    public function index()
    {

        $query = Logs::query();
        $grid = new Grid(
            (new GridConfig)
                ->setDataProvider(
                    new EloquentDataProvider($query)
                )
                ->setName('swapper')
                ->setPageSize(30)
                ->setColumns([
                    (new FieldConfig)
                        ->setName('id')
                        ->setLabel('LogId')
                        ->setSortable(true)
                        ->addFilter(
                            (new FilterConfig)
                                ->setOperator(FilterConfig::OPERATOR_LIKE)
                        )
                    ,

                    (new FieldConfig)
                        ->setName('statement')
                        ->setLabel('Statement')
                        ->setSortable(true)
                        ->addFilter(
                            (new FilterConfig)
                                ->setOperator(FilterConfig::OPERATOR_LIKE)
                        )
                    ,
                    (new FieldConfig)
                        ->setName('time')
                        ->setLabel('LogTime')
                        ->setSortable(true)

                    ,


                ])
                ->setComponents([
                    (new THead)
                        ->setComponents([
                            //    (new HtmlTag)->setAttributes(['class' => 'float-right'])->addComponent(new Pager),
                            (new HtmlTag)
                                ->setAttributes(['class' => 'float-left pb-2'])
                                ->addComponent(new ShowingRecords)
                            ,
                            (new ColumnHeadersRow),
                            (new FiltersRow)
                                ->addComponents([
                                    (new RenderFunc(function () {
                                        return "<style>
                                                .daterangepicker td.available.active,
                                                .daterangepicker li.active,
                                                .daterangepicker li:hover {
                                                    color:black !important;
                                                    font-weight: bold;
                                                }
                                           </style>";
                                    }))
                                        ->setRenderSection('filters_row_column_time'),
                                    (new DateRangePicker)
                                        ->setName('time')
                                        ->setRenderSection('filters_row_column_time')
                                        ->setDefaultValue([date("Y-m-d", strtotime("-12 months")), date('Y-m-d')])
                                ]),
                            (new OneCellRow)
                                ->setRenderSection(RenderableRegistry::SECTION_END)
                                ->setComponents([
                                    new RecordsPerPage,
                                    //    new ColumnsHider(),
                                    (new CsvExport)->setFileName('logs' . date('Y-m-d'))->setRowsLimit
                                    (50000000),
                                    (new ExcelExport)->setFileName('logs' . date('Y-m-d'))->setRowsLimit(50000000),
                                    (new HtmlTag)
                                        ->setContent('<span class="fas fa-refresh"></span> Filter')
                                        ->setTagName('button')
                                        ->setRenderSection(RenderableRegistry::SECTION_END)
                                        ->setAttributes([
                                            'class' => 'btn btn-success btn-sm'
                                        ])
                                ])
                        ])
                    ,
                    (new TFoot)
                        ->setComponents([

                            (new OneCellRow)
                                ->setComponents([
                                    new Pager,
                                    (new HtmlTag)
                                        ->setAttributes(['class' => 'pull-right'])
                                        ->addComponent(new ShowingRecords)
                                    ,
                                ])
                        ])
                    ,
                ])
        );

        $grid = $grid->render();
        return view('logs.index', compact('grid'));
    }

}
