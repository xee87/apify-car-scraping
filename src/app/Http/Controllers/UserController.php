<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Faker\Provider\DateTime;
use App\Models\Scraped_data;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use Spatie\Permission\Models\Role;

class UserController extends Controller
{


    public function index()
    {
        $users = User::where('status', '!=', 2)->get();
        return view('users.index', compact('users'));
    }

    public function add(){
        $roles = Role::all();
        return view('users.form', compact('roles'));
    }

    public function edit($id){
        $roles = Role::all();
        $user = User::where('id', $id)->first();
        return view('users.form', compact('user', 'roles'));
    }

    public function status_active(User $id)
    {

        $id->update([
            "status" => 1

        ]);
        return redirect()->route('users')->with('status', 'User Status Active!');
    }

    protected function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|min:2|max:255',
            'email' => 'required',
            'password' => 'required|string|min:4|confirmed',
            'password_confirmation' => 'required_with:password|same:password|min:4'
        ]);

        $request['allowed_postcodes'] = isset($request->allowed_postcodes) ? implode(',', $request->allowed_postcodes) : null;

        $user = User::where('email', '=', $request['email'])->first();
        $request['password'] = Hash::make($request['password']);
        if ($user === null) {
            $user = User::create($request->all());
            $user->assignRole($request['role']);
            return redirect()->route('users')->with('message', 'User Created Successfully!!');
        }else{
            return redirect()->back()->with('status', 'User Already Exist!');
        }

    }

    public function update(Request $request, User $id)
    {
        $this->validate($request, [
            'name' => 'required|min:4|max:255',
            'email' => 'required',
            'password' => ($request['password']) ? 'string|min:6|confirmed' : '',
        ]);

        $data = [
            'name' => $request['name'],
            'email' => $request['email'],
            'password' => Hash::make($request['password']),
            'allowed_postcodes' => isset($request['allowed_postcodes']) ? implode(',', $request['allowed_postcodes']) : null,
        ];

        if(empty($request['password'])){
            unset($data['password']);
        }

        $id->update($data);


        $id->syncRoles($request['role']);
        return redirect()->back()->with('status', 'User Updated Successfully!');
    }


    public function status_inactive(User $id)
    {
        $id->update([
            "status" => 0
        ]);
        return redirect()->route('users')->with('status', 'User Status Inactive!');
    }

    public function status_delete(User $id)
    {
        $id->update([
            "status" => 2
        ]);
        return redirect()->route('users')->with('status', 'User Deleted!');
    }





}
