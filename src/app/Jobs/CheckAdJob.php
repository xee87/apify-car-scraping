<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Models\Unique_data;
use App\Models\Swap_list;
use App\Helper\CommonHelper;
use App\Models\Logs;
use Mockery\Exception;

class CheckAdJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */

    protected $records;

    public function __construct()
    {
        //$this->records = Unique_data::where('status', 1)->limit(500)->get();

//        Unique_data::where('status', 1)->chunk(500, function($data){
//            $this->records = $data;
//        });
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try{
           // $records = $this->records;
            Unique_data::where('status', 1)->chunk(500, function($records){
                foreach($records as $key => $record){
                    if(!empty($record->AdUrl)){
                        $url = (strpos( $record->AdUrl, 'https://' ) !== false) ? $record->AdUrl: 'https://' . $record->AdUrl;
                        if(CommonHelper::isUrlExists($url, $record->AdId)){
                            // Logs::create(['statement' => 'Active '. $record->id]);
                            Unique_data::where('id', $record->id)->update(['count' => $record->count + 1]);
                        }else{
                            Unique_data::where('id', $record->id)->update(['status' => 0]);
                        }
                    }else{
                        Unique_data::where('id', $record->id)->update(['status' => 0]);
                    }
                    //sleep(1);
                    Logs::create(['statement' => 'Sleeping Now: ' . date('Y-m-d H:i:s')]);
                }
            });
        }catch (Exception $e){
            Logs::create(['statement' => $e->getMessage()]);
        }
    }
}
