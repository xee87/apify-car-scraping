<?php
/**
 * Created by PhpStorm.
 * User: smart comp
 * Date: 6/1/2019
 * Time: 2:56 PM
 */
namespace App\Helper;

use App\Models\User;
use Goutte;
use Illuminate\Support\Facades\DB;

class CommonHelper{


    public static function successAlert($message, $type='success'){

        ?>
        <div id="success-alert" class="alert alert-<?php echo $type ?>" style="background-color:#28a745">

            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>

            <h3 style="color:#fff"><strong><i class="fa fa-check" aria-hidden="true"></i></strong> Alert!</h3>

            <p style="color:#fff"><?php echo $message; ?></p>

        </div>
        <?php

    }

    public static function isUrlExists($url, $AdId){


        $crawler = Goutte::request('GET', $url);
        $selector = self::getAdSelector($AdId);

        $count = $crawler->filter($selector)->count();

        if($count == 0){
            return false;
        }

        return true;

    }

    public static function getAllowedPostCodes(){
        $user = User::where('id', auth()->user()->id)->first();
        if($user->allowed_postcodes){
            return explode(',', $user->allowed_postcodes);
        }
        return false;
    }

    public static function isAdmin(){
        return auth()->user()->id == 1 ? true:false;
    }

    public static function getAdSelector($AdId){


        $selector = '';

        switch($AdId){
            case '1':
                $selector = '#viewad-main';
                break;
            case '2':
                $selector = '#expose-title';
                break;
            case '3':
                $selector = '#expose';
                break;
            case '4':
                $selector = '#expose-headline';
                break;
        }

        return $selector;

    }


    public static function swapFiltersModal($add = 0){

        ?>

        <div class="modal fade show" id="<?php  echo ($add)?'modal-create':'modal-update' ?>"  aria-modal="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">

                        <h4 class="modal-title"><?php echo ($add)?'Create':'Update' ?> Filter</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>


                    </div>
                    <form method="post" action="<?php echo ($add)?route('add_filter'):route('update_filter') ?>" enctype="multipart/form-data">
                        <?php echo csrf_field(); ?>
                        <div class="modal-body">

                            <div class="row">

                                <div class="col-md-12">

                                    <div class="form-group">
                                        <label for="category">Old Value</label>
                                        <input class="fil-old form-control" type="text" name="old_value" value=""  placeholder="Old value" required>
                                        <input class="fil-id" type="hidden" name="id" value="">
                                    </div>

                                    <div class="form-group">
                                        <label for="category">New Value</label>
                                        <input class="fil-new form-control" type="text" name="new_value" value=""  placeholder="New value" required>
                                    </div>


                                </div>
                            </div>

                        </div>

                        <div class="modal-footer justify-content-between">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-info  " style="padding-left:6% ;padding-right:6% ">
                                <?php echo ($add)?'Create':'Update' ?>
                            </button>
                        </div>
                    </form>
                </div>

            </div>

        </div>


        <?php

    }




}
