<?php

namespace App\Providers;

use App\Exports\GridExport;
use App\Grids\DbalDataProvider;
use App\Grids\ExcelExport;
use Illuminate\Foundation\AliasLoader;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use Nayjest\Grids\EloquentDataProvider;
use Nayjest\Grids\Grid;
use Nayjest\Grids\Components\ExcelExport as NayjestExcelExport;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $loader = AliasLoader::getInstance();
        $loader->alias(EloquentDataProvider::class, \App\Grids\EloquentDataProvider::class);
        $loader->alias(Grid::class, \App\Grids\Grid::class);
        $loader->alias(DbalDataProvider::class, \App\Grids\DbalDataProvider::class);
        $loader->alias(NayjestExcelExport::class, ExcelExport::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
    }
}
