<?php
namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromArray;

class GridExport implements FromArray
{
    protected $data = array();

    public function __construct(array $data)
    {
        $this->data = $data;
    }

    public function array(): array
    {
        return $this->data;
    }
}
