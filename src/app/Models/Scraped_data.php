<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Scraped_data extends Model
{
	protected $updated_at = false;
	protected $created_at = false;
    public $timestamps = false;

    protected $fillable = [
        "AdId",
        "AdUrl",
        "AdName",
        "AdUrl",
        "AdLZ",
        "AdKm",
        "AdYear",
        "AdPublTime",
        "AdPrice",
        "FromUrl",
        'ExportTimeStamp',
        "BatchId",
    ];

    protected $table ="scraped_data";

}
