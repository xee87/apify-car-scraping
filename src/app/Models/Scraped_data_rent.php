<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Scraped_data_rent extends Model
{
	protected $updated_at = false;
	protected $created_at = false;
    public $timestamps = false;

    protected $fillable = [
        "AdId", "AdUrl", "AdInfo", "AdPrice", "AdPostCode", "AdDistrict",
        "AdRooms", "AdSquaremeters", "AdDate", "AdTime",'AdCity', "AdPricePerSquaremeters",
        "FromUrl", "ExportTimeStamp", "BatchId", "AdName", "AdRegionUrl", "AdSonstiges"
    ];

    protected $table ="scraped_data_rent";

//    public function category()
//    {
//        return $this->belongsTo('App\Category');
//    }
}
