<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Unique_data_report extends Model
{
	protected $updated_at = false;
	protected $created_at = false;
    public $timestamps = false;

    protected $fillable = [
        "unique_data_id",
        "AdId",
        "AdUrl",
        "AdName",
        "AdUrl",
        "AdLZ",
        "AdKm",
        "AdYear",
        "AdPublTime",
        "AdPrice",
        "FromUrl",
        'ExportTimeStamp',
        "BatchId",
    ];

    protected $table ="unique_data_report";

}
