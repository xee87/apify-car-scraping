<nav class="navbar navbar-vertical fixed-left navbar-expand-md navbar-light bg-white" id="sidenav-main">
    <div class="container-fluid">
        <!-- Toggler -->
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#sidenav-collapse-main" aria-controls="sidenav-main" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <!-- Brand -->
        <a class="navbar-brand pt-0" href="{{ route('home') }}">
            <img src="{{ asset('assets/img/a_logo.png') }}" class="navbar-brand-img" alt="...">
        </a>
        <!-- User -->
        <ul class="nav align-items-center d-md-none">
            <li class="nav-item dropdown">
                <a class="nav-link" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <div class="media align-items-center">
                        <span class="avatar avatar-sm rounded-circle">
                        <img alt="Image placeholder" src="{{ asset('argon') }}/img/theme/team-1-800x800.jpg">
                        </span>
                    </div>
                </a>
                <div class="dropdown-menu dropdown-menu-arrow dropdown-menu-right">
                    <div class=" dropdown-header noti-title">
                        <h6 class="text-overflow m-0">{{ __('Welcome!') }}</h6>
                    </div>
                    <a href="{{ route('profile.edit') }}" class="dropdown-item">
                        <i class="ni ni-single-02"></i>
                        <span>{{ __('My profile') }}</span>
                    </a>
                    <a href="#" class="dropdown-item">
                        <i class="ni ni-settings-gear-65"></i>
                        <span>{{ __('Settings') }}</span>
                    </a>
                    <a href="#" class="dropdown-item">
                        <i class="ni ni-calendar-grid-58"></i>
                        <span>{{ __('Activity') }}</span>
                    </a>
                    <a href="#" class="dropdown-item">
                        <i class="ni ni-support-16"></i>
                        <span>{{ __('Support') }}</span>
                    </a>
                    <div class="dropdown-divider"></div>
                    <a href="{{ route('logout') }}" class="dropdown-item" onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();">
                        <i class="ni ni-user-run"></i>
                        <span>{{ __('Logout') }}</span>
                    </a>
                </div>
            </li>
        </ul>
        <!-- Collapse -->
        <div class="collapse navbar-collapse" id="sidenav-collapse-main">
            <!-- Collapse header -->
            <div class="navbar-collapse-header d-md-none">
                <div class="row">
                    <div class="col-6 collapse-brand">
                        <a href="{{ route('home') }}">
                            <img src="{{ asset('argon') }}/img/brand/blue.png">
                        </a>
                    </div>
                    <div class="col-6 collapse-close">
                        <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#sidenav-collapse-main" aria-controls="sidenav-main" aria-expanded="false" aria-label="Toggle sidenav">
                            <span></span>
                            <span></span>
                        </button>
                    </div>
                </div>
            </div>
            <!-- Form -->
            <form class="mt-4 mb-3 d-md-none">
                <div class="input-group input-group-rounded input-group-merge">
                    <input type="search" class="form-control form-control-rounded form-control-prepended" placeholder="{{ __('Search') }}" aria-label="Search">
                    <div class="input-group-prepend">
                        <div class="input-group-text">
                            <span class="fa fa-search"></span>
                        </div>
                    </div>
                </div>
            </form>
            <!-- Navigation -->

            <ul class="navbar-nav">
                @role('Admin')
                <li class="nav-item {{ Request::segment(2) === 'users' ? 'active' : '' }}">
                    <a class="nav-link" href="{{ route('users') }}">
                        <i class="fa fa-users text-primary"></i> {{ __('Users') }}
                    </a>
                </li>
                <li class="nav-item {{ Request::segment(2) === 'swap-lists' ? 'active' : '' }}">
                    <a class="nav-link" href="{{ route('show_swap_lists') }}">
                        <i class="fa fa-exchange-alt text-primary"></i> {{ __('Swapper List') }}
                    </a>
                </li>
                <li class="nav-item {{ Request::segment(2) === 'scraped-data' ? 'active' : '' }}">
                    <a class="nav-link" href="{{ route('scraped_data') }}">
                        <i class="far fa-clone text-primary"></i> {{ __('Scrapped Data') }}
                    </a>
                </li>
                <li class="nav-item {{ Request::segment(2) === 'unique-data' ? 'active' : '' }}">
                    <a class="nav-link" href="{{ route('unique_data') }}">
                        <i class="fas fa-sync-alt text-primary"></i> {{ __('Unique Data') }}
                    </a>
                </li>
                @endrole
                <li class="nav-item {{ Request::segment(2) === 'unique-data-report' ? 'active' : '' }}">
                    <a class="nav-link" href="{{ route('unique_data_report') }}">
                        <i class="fas fa-chart-line text-primary"></i> {{ __('Unique Data Report') }}
                    </a>
                </li>
                <li class="nav-item {{ Request::segment(2) === 'visual-query' ? 'active' : '' }}">
                    <a class="nav-link" href="{{ route('visualQ') }}">
                        <i class="fas fa-crosshairs text-primary"></i> {{ __('Visual Query Builder') }}
                    </a>
                </li>
                <li class="nav-item {{ Request::segment(2) === 'sql-statements' ? 'active' : '' }}">
                    <a class="nav-link" href="{{ route('sql_index') }}">
                        <i class="fas fa-database text-primary"></i> {{ __('Run SQL Query') }}
                    </a>
                </li>
                @role('Admin')
                <li class="nav-item {{ Request::segment(2) === 'logs' ? 'active' : '' }}">
                    <a class="nav-link" href="{{ route('log.index') }}">
                        <i class="fas fa-sign-in-alt text-primary"></i> {{ __('Logs') }}
                    </a>
                </li>
                @endrole
            </ul>

        </div>
    </div>
</nav>
