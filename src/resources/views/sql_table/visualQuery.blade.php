@extends('layouts.app', ['title' => __('Visual Query Builder')])

@section('content')
    @include('users.partials.header', [
        'title' => 'Visual Query Builder',
        'description' => __(''),
        'class' => 'col-lg-12'
    ])

    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col-xl-12 order-xl-1">
                <div class="card shadow">
                    <div class="card-header bg-white mb-0 border-1">
                        <div class="row align-items-center">
                            <h3 class="mb-0">Visual Query Builder</h3>
                        </div>
                    </div>
                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                {{ session('status') }}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        @endif
                        <div class="row">
                            <div class="col-md-12">
                                <button style="float: right; margin-bottom: 5px;" class="btn btn-info pull-right show-vb hide" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                                    Close Visual Builder
                                </button>
                            </div>
                        </div>

                        <form method="post" action="{{  route('sql_run') }}" autocomplete="off">
                            @csrf

                            @if(isset($error) && !empty($error))
                                <div class="error-sql-message">
                                    {{ $error  }}
                                </div>
                            @endif

                            <div class="pl-lg-4">

                                <div class="collapse show" id="collapseExample">

                                    <div class="row">

                                        <div class="col-md-6 mb-3">
                                            <label class="form-control-label">Select Table</label>
                                            <select class="getTables form-control-alternative form-control" name="table" data-href="{{ route('get_table') }}" required="">
                                                <option disabled selected>Select</option>
                                                @role('Admin')
                                                <option value="scraped_data">scraped_data</option>
                                                <option value="unique_data">unique_data</option>
                                                @endrole
                                                <option value="unique_data_report">unique_data_report</option>
                                            </select>
                                        </div>

                                        <div class="col-md-6 mb-3 hide show-on">

                                            <label class="form-control-label">Select Fields (empty for *)</label>

                                            <div class="form-group select-fields">
                                                <select placeholder="(*)" class="form-control form-control-alternative select2 tablefields" name="select_fields[]" multiple="multiple"
                                                        data-placeholder="Select Fields" style="width: 100%;" autofocus>
                                                </select>
                                            </div>

                                        </div>


                                        <div class="col-md-12 hide show-on">

                                            {{--<div class="collapse show" id="collapseExample">--}}

                                            <div class="">
                                                <div class="query-builder">
                                                    <label class="form-control-label"> Aggregation Functions </label>
                                                    <div class="table-repsonsive rules-group-container">
                                                        <span id="error"></span>
                                                        <table class="table table-bordered" id="func_table">
                                                            <tr>
                                                                <th width="30%">Field</th>
                                                                <th width="30%">Function</th>
                                                                <th width="30%">AS</th>
                                                                <th width="10%"><button type="button" name="add" class="btn btn-success btn-sm f-add"><span class="fa fa-plus"></span></button></th>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>

                                            <label class="form-control-label mt-2">Where</label>
                                            <div id="builder"></div>


                                            <div style="margin-top: 15px;" class="query-builder">
                                                <label class="form-control-label"> Results Sequence </label>
                                                <div class="table-repsonsive rules-group-container">
                                                    <span id="error"></span>
                                                    <table class="table table-bordered" id="item_table">
                                                        <tr>
                                                            <th width="40%">Field</th>
                                                            <th width="40%">Sorting Method</th>
                                                            <th width="20%"><button type="button" name="add" class="btn btn-success btn-sm add"><span class="fa fa-plus"></span></button></th>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>

                                            <div style="margin-top: 15px;" class="query-builder">
                                                <label class="form-control-label"> Results Arrangement </label>
                                                <div class="row rules-group-container">

                                                    <div class="col-md-6">

                                                        <label class="form-control-label mt-2">Group By</label>

                                                        <div class="form-group group-by">
                                                            <select placeholder="" class="form-control form-control-alternative select2 tablefields" name="group_by[]" multiple="multiple"
                                                                    data-placeholder="Select Fields" style="width: 100%;" autofocus>
                                                            </select>
                                                        </div>

                                                    </div>

                                                    <div class="col-md-6">
                                                        <label class="form-control-label mt-2">Limit</label>

                                                        <div class="form-group">
                                                            <select placeholder="Limit" class="form-control form-control-alternative query-limit" name="limit"
                                                                    style="width: 100%;" autofocus>
                                                                {{--<option value="" selected>Select Limit</option>--}}
                                                                <option value="50">50</option>
                                                                <option value="100">100</option>
                                                                <option value="500" selected>500</option>
                                                                <option value="1000">1000</option>
                                                                <option value="5000">5000</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>


                                            <hr>

                                            <button type="button" class="show-on btn btn-success mt-2 click-button hide" data-href="{{ route('test_sql') }}" style=" float: right">
                                                <i class="fas fa-sync-alt fa-spin hide" style="font-size:18px"></i>
                                                <p class="spin-text" style="display: inline;">RUN QUERY</p>
                                            </button>
                                            {{--</div>--}}

                                        </div>


                                    </div>

                                </div>
                            </div>
                        </form>

                        <br>

                        <div class="table-responsive">
                            <p class="total-records hide">Total Records: </p>
                            <table id="ajax-table" class="table table-bordered table-hover">
                                <thead class="thead-light">
                                <tr></tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                        <p class="no-record hide">No Record Found!</p>


                    </div>
                </div>
            </div>
        </div>

        <table class="clone-select hide">
            <tr>
                <td width="40%">
                    <select name="sorting_fileds[]" class="form-control tablefields">
                        <option></option>
                    </select>
                </td>
                <td width="40%">
                    <select name="sorting[]" class="form-control sort-method"><option value="ASC" selected="selected">ASC</option><option value="DESC">DESC</option></select>
                </td>
                <td width="20%">
                    <button type="button" name="remove" class="btn btn-danger btn-sm remove"><span class="fa fa-minus"></span></button>
                </td>
            </tr>
        </table>

        <table class="clone-agg-func hide">
            <tr>
                <td width="30%">
                    <select name="agg_fileds[]" class="form-control tablefields">
                        <option></option>
                    </select>
                </td>
                <td width="30%">
                    <select name="functions[]" class="form-control functions">
                        <option value="count" selected="selected">Count()</option>
                        <option value="sum">Sum()</option>
                        <option value="avg">Avg()</option>
                        <option value="min">Min()</option>
                        <option value="max">Max()</option>
                    </select>
                </td>
                <td width="30%"><input placeholder="columnName" type="text" name="as[]" class="form-control as-text"></td>
                <td width="10%">
                    <button type="button" name="remove" class="btn btn-danger btn-sm f-remove"><span class="fa fa-minus"></span></button>
                </td>
            </tr>
        </table>

        @include('layouts.footers.auth')
    </div>
@endsection
