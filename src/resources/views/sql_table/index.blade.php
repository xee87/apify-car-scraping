@extends('layouts.app', ['title' => __('SQL Statements')])

@section('content')
    @include('users.partials.header', [
        'title' => 'SQL Statements',
        'description' => __(''),
        'class' => 'col-lg-12'
    ])

    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col-xl-12 order-xl-1">
                <div class="card shadow">
                    <div class="card-header bg-white mb-0 border-1">
                        <div class="row align-items-center">
                            <h3 class="mb-0">SQL Statements</h3>
                        </div>
                    </div>
                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                {{ session('status') }}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        @endif
                        <form method="post" action="{{  route('sql_run') }}" autocomplete="off">
                            @csrf

                            @if(isset($error) && !empty($error))
                                <div class="error-sql-message">
                                    {{ $error  }}
                                </div>
                            @endif

                            <div class="pl-lg-4">

                                <div class="row">

                                    <div class="col-md-12">

                                        <div class="form-group">
                                            <label class="form-control-label" for=""> CAUTION! It will run queries on databases directly.</label>
                                            <textarea rows="7" style="{{ (isset($error) && !empty($error))?'color:red !important;':'sd'  }}" name="sql_statement" class="form-control form-control-alternative" id=""
                                                      placeholder="SQL QUERY" required>{{ (isset($sql_statement))?$sql_statement:''  }}</textarea>
                                        </div>

                                    </div>

                                </div>
                            </div>

                            <div class="text-right">
                                <button type="submit" class="btn btn-success mt-2">RUN</button>
                            </div>
                        </form>

                            <br>

                            @if(isset($results))

                                @if(is_array($results) && !empty($results))


                                    <div class="table-responsive">

                                        <table id="user_table" class="table table-bordered table-hover">

                                            <thead class="thead-light">

                                            <tr>
                                                @foreach($results[0] as $k => $data)
                                                    <th style="width: 25%">{{$k}}</th>
                                                @endforeach
                                            </tr>

                                            </thead>

                                            <tbody>

                                            @foreach($results as $k => $data)
                                                <tr>
                                                    @foreach($data as $k => $v)
                                                        <td>{{$v}}</td>
                                                    @endforeach
                                                </tr>
                                            @endforeach

                                            </tbody>

                                        </table>

                                    </div>

                                @else
                                    @if(empty($results) && empty($error))
                                        <p>No record found</p>
                                    @endif
                                @endif

                            @endif

                    </div>
                </div>
            </div>
        </div>

        @include('layouts.footers.auth')
    </div>
@endsection
