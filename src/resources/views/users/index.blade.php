@extends('layouts.app')

@section('content')
    @include('users.partials.header', [
        'title' => __('Users'),
        'description' => __(''),
        'class' => 'col-lg-12'
    ])
<div class="container-fluid mt--7">
    <div class="row">
        <div class="col">
            <div class="card shadow">
                <div class="card-header border-0">
                    @if (session('status'))
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            {{ session('status') }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    @endif
                    <div class="row align-items-center">
                        <div class="col-8">
                            <h3 class="mb-0">Users</h3>
                        </div>
                        <div class="col-4 text-right">
                            <a href="{{ route('user.add') }}" class="btn btn-sm btn-primary">Add User</a>
                        </div>
                    </div>
                </div>

                <div class="col-12">
                </div>

                <div class="table-responsive">
                    <table id="user_table" class="table align-items-center table-flush">
                        <thead class="thead-light">
                        <tr>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Date Added</th>
                            <th>Role</th>
                            <th>Status</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($users as $user)
                        <tr>
                            <td>{{$user->name}}</td>

                            <td>{{$user->email}}</td>
                            @php
                                $originalDate = $user->created_at;
                                $newDate = date("M d, Y h:i a", strtotime($originalDate));
                            @endphp
                            <td>{{$newDate}}</td>
                            <td>{{$user->getRoleNames()->first()}}</td>
                            <td>{{($user->status == 1) ? 'Active': 'Inactive'}}</td>
                            <td class="text-right">
                                <div class="dropdown">
                                    <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="fas fa-ellipsis-v"></i>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                        <a class="dropdown-item" href="{{ route('user.edit', ['id' => $user->id]) }}">Edit</a>
                                        @if($user->id != auth()->user()->id)
                                        <a class="dropdown-item confirm-del" href="{{ route('user.delete', ['id' => $user->id]) }}">Delete</a>
                                        @endif
                                    </div>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="card-footer py-4">
                    <nav class="d-flex justify-content-end" aria-label="...">

                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
