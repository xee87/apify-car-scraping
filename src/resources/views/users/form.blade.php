@extends('layouts.app', ['title' => __('Client Claim')])

@php
$edit = isset($user) ? true:false;
@endphp

@section('content')
    @include('users.partials.header', [
        'title' => $edit ? 'Update User':'Create User',
        'description' => __(''),
        'class' => 'col-lg-12'
    ])

    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col-xl-12 order-xl-1">
                <div class="card bg-secondary shadow">
                    <div class="card-header bg-white border-0">
                        <div class="row align-items-center">
                            <h3 class="mb-0">{{ $edit ? 'Update':'Create' }} User</h3>
                        </div>
                    </div>
                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                {{ session('status') }}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        @endif
                        <form method="post" action="{{ $edit ? route('user.update', ['id' => $user->id]) : route('user.store') }}" autocomplete="off">
                            @csrf

{{--                            <h6 class="heading-small text-muted mb-4">{{ __('Customer Details') }}</h6>--}}
                            <div class="pl-lg-4">

                                <div class="row">
                                    <div class="col-6">
                                        <div class="form-group{{ $errors->has('name') ? ' has-danger' : '' }}">
                                            <label class="form-control-label" for="input-name">{{ __('Name') }}</label>
                                            <input type="text" name="name" id="input-name" class="form-control form-control-alternative{{ $errors->has('name') ? ' is-invalid' : '' }}" placeholder="{{ __('Name') }}" value="{{ old('name', $edit ? $user->name : '') }}" required autofocus>

                                            @if ($errors->has('name'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('name') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="col-6">
                                        <div class="form-group{{ $errors->has('email') ? ' has-danger' : '' }}">
                                            <label class="form-control-label" for="input-name">{{ __('Email') }}</label>
                                            <input type="email" name="email" id="input-name" class="form-control form-control-alternative{{ $errors->has('email') ? ' is-invalid' : '' }}" placeholder="{{ __('Email') }}" value="{{ old('email', $edit ? $user->email : '') }}" required autofocus>

                                            @if ($errors->has('email'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('email') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>


                                    <div class="col-6">
                                        <div class="form-group{{ $errors->has('password') ? ' has-danger' : '' }}">
                                            <label class="form-control-label" for="input-name">{{ __('Password') }}</label>
                                            <input type="password" name="password" id="input-name" class="form-control form-control-alternative {{ $errors->has('password') ? ' is-invalid' : '' }}" placeholder="{{ __('Password') }}" value="{{ old('password') }}" @if(!$edit) required @endif autofocus>
                                            <small>Leave empty to keep the same</small>

                                            @if ($errors->has('password'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('password') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="col-6">
                                        <div class="form-group{{ $errors->has('password_confirmation') ? ' has-danger' : '' }}">
                                            <label class="form-control-label" for="input-name">{{ __('Retype password') }}</label>
                                            <input type="password" name="password_confirmation" id="input-name" class="form-control form-control-alternative{{ $errors->has('password_confirmation') ? ' is-invalid' : '' }}" placeholder="{{ __('Retype password') }}" value="{{ old('password_confirmation') }}" @if(!$edit) required @endif autofocus>
                                            <small>Leave empty to keep the same</small>

                                            @if ($errors->has('password_confirmation'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('password_confirmation') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>

{{--                                    @if($edit && $user->id != 1)--}}

                                    <div class="col-6">
                                        <div class="form-group{{ $errors->has('allowed_postcodes') ? ' has-danger' : '' }}">
                                            <label class="form-control-label" for="input-name">
                                                {{ __('Allowed Postcodes') }}
                                            </label>
                                            <select name="allowed_postcodes[]"
                                                    multiple="multiple"
                                                    data-placeholder="Enter Postcodes"
                                                    style="width: 100%;" autofocus
                                                    class="form-control form-control-alternative tags2"
                                                    id="input-name">
                                                @if($edit && $user->allowed_postcodes)
                                                    @php $options = explode(',',$user->allowed_postcodes); @endphp
                                                    @foreach($options as $option)
                                                        <option value="{{ $option }}" selected>{{ $option }}</option>
                                                    @endforeach
                                                @endif
                                                    <option value=""></option>
                                            </select>
                                            <small>Type & Press enter for new postcode</small>
                                            @if ($errors->has('allowed_postcodes'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('allowed_postcodes') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="col-6">
                                        <div class="form-group{{ $errors->has('telephone_number') ? ' has-danger' : '' }}">
                                            <label class="form-control-label" for="user_role">{{ __('Role') }}</label>
                                            <select onchange="checkmyuser()" class="form-control form-control-alternative" name="role" id="user_role">
                                                @if($edit)
                                                <option>{{$user->getRoleNames()->pop()}}</option>
                                                @endif
                                                @foreach($roles as $role)
                                                    @if($edit && $user->getRoleNames()->pop()!= $role->name)
                                                    <option>{{$role->name}}</option>
                                                    @endif
                                                    <option>{{$role->name}}</option>
                                                @endforeach
                                            </select>

                                            @if ($errors->has('role'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('role') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>

{{--                                    @endif--}}


                                </div>
                            </div>

                            <div class="text-center">
                                <button type="submit" class="btn btn-success mt-4">{{ $edit ? 'Update':'Create' }}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        @include('layouts.footers.auth')
    </div>
@endsection
