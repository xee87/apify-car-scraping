@extends('layouts.app', ['title' => __('Unique Data Report')])

@section('content')
    @include('users.partials.header', [
        'title' => isset($edit) ? 'Update Unique Data Report':'Create Unique Data Report',
        'description' => __(''),
        'class' => 'col-lg-12'
    ])

    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col-xl-12 order-xl-1">
                <div class="card bg-secondary shadow">
                    <div class="card-header bg-white border-0">
                        <div class="row align-items-center">
                            <h3 class="mb-0">{{ isset($edit) ? 'Update':'Create' }} Unique Data Report</h3>
                        </div>
                    </div>
                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                {{ session('status') }}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        @endif
                        <form method="post" action="{{ (isset($edit))?route('update_unique_data_report', ['id' => $edit->id]):route('add_unique_data_report') }}" autocomplete="off">
                            @csrf

{{--                            <h6 class="heading-small text-muted mb-4">{{ __('Customer Details') }}</h6>--}}
                            <div class="pl-lg-4">

                                <div class="row">

                                    <div class="col-md-4">

                                        <div class="form-group">
                                            <label class="form-control-label" for="exampleInputEmail1">Unique Data Id</label>
                                            <input type="text" name="unique_data_id" value="{{ (isset($edit))?$edit->unique_data_id:'' }}" class="form-control form-control-alternative" id="" placeholder="Unique Data Id" required>
                                        </div>

                                    </div>

                                    <div class="col-md-4">

                                        <div class="form-group">
                                            <label class="form-control-label" for="exampleInputEmail1">Ad Id</label>
                                            <input type="number" name="AdId" value="{{ (isset($edit))?$edit->AdId:'' }}" class="form-control form-control-alternative" id="" placeholder="Ad Id" required>
                                        </div>

                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="form-control-label">Ad Name</label>
                                            <input type="text" name="AdName" value="{{ (isset($edit))?$edit->AdName:'' }}" class="form-control form-control-alternative" id="" placeholder="Ad Name" required>
                                        </div>
                                    </div>

                                    <div class="col-md-4">

                                        <div class="form-group">
                                            <label class="form-control-label">Ad Url</label>
                                            <input type="text" name="AdUrl" value="{{ (isset($edit))?$edit->AdUrl:'' }}" class="form-control form-control-alternative" id="" placeholder="Ad URL" required>
                                        </div>
                                    </div>

                                    <div class="col-md-4">

                                        <div class="form-group">
                                            <label class="form-control-label">Ad LZ</label>
                                            <input type="text" name="AdLZ" value="{{ (isset($edit))?$edit->AdLZ:'' }}" class="form-control form-control-alternative" id="" placeholder="Ad LZ" required>
                                        </div>

                                    </div>

                                    <div class="col-md-4">

                                        <div class="form-group">
                                            <label class="form-control-label">Ad Km</label>
                                            <input type="text" name="AdKm" value="{{ (isset($edit))?$edit->AdKm:'' }}" class="form-control form-control-alternative" id="" placeholder="Ad Km" required>
                                        </div>
                                    </div>

                                    <div class="col-md-4">

                                        <div class="form-group">
                                            <label class="form-control-label">Ad Year</label>
                                            <input type="text" name="AdYear" value="{{ (isset($edit))?$edit->AdYear:'' }}" class="form-control form-control-alternative" id="" placeholder="Ad Year" required>
                                        </div>
                                    </div>

                                    <div class="col-md-4">

                                        <div class="form-group">
                                            <label class="form-control-label">Ad Publication Time</label>
                                            <input type="text" name="AdPublTime" value="{{ (isset($edit))?$edit->AdPublTime:'' }}" class="form-control form-control-alternative" id="" placeholder="Ad Publication Time" required>
                                        </div>

                                    </div>

                                    <div class="col-md-4">

                                        <div class="form-group">
                                            <label class="form-control-label">Ad Price</label>
                                            <input type="text" name="AdPrice" value="{{ (isset($edit))?$edit->AdPrice:'' }}" class="form-control form-control-alternative" id="" placeholder="Ad Price" required>
                                        </div>
                                    </div>

                                    <div class="col-md-4">

                                        <div class="form-group">
                                            <label class="form-control-label">Ad Name</label>
                                            <input type="text" name="AdName" value="{{ (isset($edit))?$edit->AdName:'' }}" class="form-control form-control-alternative" id="" placeholder="Ad URL" required>
                                        </div>
                                    </div>

                                    <div class="col-md-4">

                                        <div class="form-group">
                                            <label class="form-control-label">From Url</label>
                                            <input type="text" name="FromUrl" value="{{ (isset($edit))?$edit->FromUrl:'' }}" class="form-control form-control-alternative" id="" placeholder="From URL" required>
                                        </div>

                                    </div>

                                    <div class="col-md-4">

                                        <div class="form-group">
                                            <label class="form-control-label">ExportTimeStamp</label>
                                            <input type="text" name="ExportTimeStamp" value="{{ (isset($edit))?$edit->ExportTimeStamp:'' }}" class="form-control form-control-alternative" id="" placeholder="Ad URL" required>
                                        </div>
                                    </div>

                                    <div class="col-md-4">

                                        <div class="form-group">
                                            <label class="form-control-label">Batch Id</label>
                                            <input type="text" name="BatchId" value="{{ (isset($edit))?$edit->BatchId:'' }}" class="form-control form-control-alternative" id="" placeholder="Batch Id" required>
                                        </div>
                                    </div>




                                </div>
                            </div>

                            <div class="text-center">
                                <button type="submit" class="btn btn-success mt-4">{{ isset($edit) ? 'Update':'Create' }}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        @include('layouts.footers.auth')
    </div>
@endsection
