if (typeof jQuery !== 'undefined')(function (window, document, $, undefined) {

    //"use normal";

    var
        self = '',
        selectedId = [],
        exactAdresLocation = '',
        test1 = '',
        geocoder = {},
        mapLatLng = '',
        completeLocationsData = [],
        dummyArray = [],
        mapBounds = '',
        map = '',
        lastMapsInterval = null,
        clicked = false,

        APIFY = {

            init: function () {
                self = this;
                self.editFormPopUp();
                self.recordsPerPage();
                //self.queryBuilder();
                self.getTableAttributes();
                self.dynamicSortingOptions();
                self.functionSortingOptions();
                self.runQuery();
                self.gridLongTextHandle();
                self.submitMapForm();
                self.selectPostCodes();


                //    self.mapMarkers();
                $('.select2').select2();
                $(".tags2").select2({
                    tags: true,
                    tokenSeparators: [',', ' '],
                    dropdownCss: {display: 'none'}
                });
                $('#user_table').dataTable({
                    "aLengthMenu": [[25, 50, 500, 1000, -1], [25, 50, 500, 1000, "All"]],
                    "iDisplayLength": 500,
                    dom: 'lBfrtip',
                    buttons: [
                        {
                            extend: 'csv',
                            text: 'Export CSV',
                            charset: 'utf-8',
                            extension: '.csv',
                            fieldSeparator: ';',
                            fieldBoundary: '',
                            filename: 'export',
                            bom: true
                        },
                        {
                            extend: 'excel',
                            text: 'Export Excel',
                            charset: 'utf-8',
                            extension: '.xlsx',
                            fieldSeparator: ';',
                            fieldBoundary: '',
                            filename: 'export',
                            bom: true
                        }
                    ]
                });

                $('thead').addClass('thead-light');

                if (typeof google !== 'undefined') {
                    geocoder =   new google.maps.Geocoder();
                }
               // self.getMapLive();


            },

            gridLongTextHandle: function(){

                var tableGrid = $('#swapper');
                tableGrid.find('.see-more').on('click', function (e) {
                    e.preventDefault();
                    $(this).parent().addClass('hide');
                    $(this).closest('td').find('.full-text').removeClass('hide');
                });

                tableGrid.find('.see-less').on('click', function (e) {
                    e.preventDefault();
                    $(this).parent().addClass('hide');
                    $(this).closest('td').find('.short-text').removeClass('hide');
                })

            },

            submitMapForm: function(){

                $('#map-data-form').on('submit',function(e) {


                    var selectedIds = [];
                    $('#swapper').find('input.big-checkbox:checked').each(function() {
                        selectedIds.push($(this).val());
                    });
                    $('.checked_ids').val(selectedIds);

                    if(selectedIds.length == 0){
                        e.preventDefault();
                        alert('please make selection first by clicking checkbox');
                    }

                });

            },

            getMapLive: function(){
                if($('.datatoMap').length > 0){
                    var data = $(document).find('.datatoMap').val();
                    data = $.parseJSON(data);
                    self.postCodeTranslate(data);
                }
            },

            selectPostCodes: function(){   //not in use

                $('.locate-map-pc').on('click', function(e){
                    e.preventDefault();
                    var selectedIds = [];
                    $('#swapper').find('input.big-checkbox:checked').each(function() {
                        selectedIds.push($(this).val());
                    });

                    if(selectedIds.length > 0) {

                        $.ajax({
                            method: 'POST', // Type of response and matches what we said in the route
                            url: '/ajax-records-data', // This is the url we gave in the route
                            data: {'data': selectedIds}, // a JSON object to send back
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            success: function (response) { // What to do if we succeed
                                 console.log(response.length);
                                completeLocationsData = [];
                                try {
                                    self.postCodeTranslate(response);
                                  //  console.log(response);
                                } catch (e) {
                                    console.log(e);
                                }
                            },
                            error: function (jqXHR, textStatus, errorThrown) { // What to do if we fail
                                console.log(JSON.stringify(jqXHR));
                                console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
                            }
                        });
                    }else{
                        alert('please make selection first by clicking checkbox');
                    }
                });

            },

            postCodeTranslate: function(postCode){

                self.jntShowLoader();
              //  console.log(postCode);

                $.each(postCode, function(i,e){
                    //console.log(e);
                    geocoder.geocode(
                        {
                            'componentRestrictions': {'postalCode': i, 'country': 'de'},
                             region: "de"
                        },
                        function (results, status) {
                            if (status == google.maps.GeocoderStatus.OK) {
                                var it = results[0].address_components.length - 1;
                                if(results[0].address_components[it].short_name == 'DE'){
                                    var lat = results[0].geometry.location.lat();
                                    var lng = results[0].geometry.location.lng();
                                    var bounds = results[0].geometry.bounds;
                                    mapLatLng = new google.maps.LatLng(lat, lng);
                                    self.jntInitMap(lat,lng, i, e.length, bounds);
                                }else{
                                    console.log('Post Code: '+ i + ' does not exist in Germany.');
                                }
                            }
                        }
                    );

                });

                var itrator = 0;
                //var marker;

                if(lastMapsInterval){
                    clearInterval(lastMapsInterval);
                }

                lastMapsInterval = setInterval(function(){

                    if(itrator <=0) {

                        map = new google.maps.Map(
                            document.getElementById('map')
                            //, {center: mapLatLng, zoom: 15}
                        );

                        mapBounds = new google.maps.LatLngBounds();

                        $.each(completeLocationsData, function (i, e) {
                            $.each(postCode, function(a,b){
                                if(a == e.postCode){
                                    $.each(e.locations, function(o,c){
                                       self.jntCreateMarker(c,postCode[a][o]);

                                    });
                                }
                            });
                        });

                    }
                    if(itrator == 1){
                        map.fitBounds(mapBounds);
                    }
                    itrator++;
                }, 5000);

               // console.log(marker);

              //  self.addYourLocationButton(map, marker);

            },

            addYourLocationButton: function(map, marker){


                var controlDiv = document.createElement('div');

                var firstChild = document.createElement('button');
                firstChild.style.backgroundColor = '#fff';
                firstChild.style.border = 'none';
                firstChild.style.outline = 'none';
                firstChild.style.width = '28px';
                firstChild.style.height = '28px';
                firstChild.style.borderRadius = '2px';
                firstChild.style.boxShadow = '0 1px 4px rgba(0,0,0,0.3)';
                firstChild.style.cursor = 'pointer';
                firstChild.style.marginRight = '10px';
                firstChild.style.padding = '0px';
                firstChild.title = 'Your Location';
                controlDiv.appendChild(firstChild);

                var secondChild = document.createElement('div');
                secondChild.style.margin = '5px';
                secondChild.style.width = '18px';
                secondChild.style.height = '18px';
                secondChild.style.backgroundImage = 'url(https://maps.gstatic.com/tactile/mylocation/mylocation-sprite-1x.png)';
                secondChild.style.backgroundSize = '180px 18px';
                secondChild.style.backgroundPosition = '0px 0px';
                secondChild.style.backgroundRepeat = 'no-repeat';
                secondChild.id = 'you_location_img';
                firstChild.appendChild(secondChild);

                google.maps.event.addListener(map, 'dragend', function() {
                    $('#you_location_img').css('background-position', '0px 0px');
                });

                firstChild.addEventListener('click', function() {
                    var imgX = '0';
                    var animationInterval = setInterval(function(){
                        if(imgX == '-18') imgX = '0';
                        else imgX = '-18';
                        $('#you_location_img').css('background-position', imgX+'px 0px');
                    }, 500);
                    if(navigator.geolocation) {
                        navigator.geolocation.getCurrentPosition(function(position) {
                            var latlng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
                            marker.setPosition(latlng);
                            map.setCenter(latlng);
                            clearInterval(animationInterval);
                            $('#you_location_img').css('background-position', '-144px 0px');
                        });
                    }
                    else{
                        clearInterval(animationInterval);
                        $('#you_location_img').css('background-position', '0px 0px');
                    }
                });

                controlDiv.index = 1;
                map.controls[google.maps.ControlPosition.RIGHT_BOTTOM].push(controlDiv);

            },

            jntInitMap: function (lat, lng, postCode,recordsCount, bounds) {

                //console.log(bounds);
                //console.log(recordsCount);
                infowindow = new google.maps.InfoWindow();

                map = new google.maps.Map(
                    document.getElementById('map'), {
                        //center: mapLatLng, zoom: 11
                        //zoomControl: true,
                        //mapTypeControl: true,
                        //scaleControl: true,
                        //streetViewControl: true,
                        //rotateControl: true,
                        //fullscreenControl: true

                    }
                );


                var request = {
                    location: mapLatLng,
                    bounds: bounds,
                    //radius:5000,
                    fields: ['geometry', 'name']
                    // type: ['supermarket', 'gas_station', 'stadium', 'pharmacy']
                };

                service = new google.maps.places.PlacesService(map);

                service.nearbySearch(request, function (results, status) {
                    if (status === google.maps.places.PlacesServiceStatus.OK) {

                      //  console.log(results);
                        var locationsToInclude = [];
                        var found = true;
                        for(rc=0; rc<recordsCount; rc++){

                            $.each(dummyArray, function(i,e){
                                if(e == results[rc].place_id){
                                    recordsCount = recordsCount+1;
                                    found = false;
                                    return false;
                                }
                                found = true;
                               // console.log(e.locations[0].place_id + ' ' + e.postCode);
                            });

                            if(found){
                                dummyArray.push(results[rc].place_id);
                                locationsToInclude.push(results[rc]);
                            }

                            //console.log(results[rc]);
                        }
                        completeLocationsData.push({'postCode':postCode, locations:locationsToInclude});
                        //completeLocationsData.push(results);
                       // map.setCenter(results[0].geometry.location);
                    }else{
                        console.log(status);
                    }
                });

            },

            jntCreateMarker: function (place, recordDetails) {

                var marker = new google.maps.Marker({
                    map: map,
                    position: place.geometry.location
                 //   position: new google.maps.LatLng(place.geometry.location.lat(),place.geometry.location.lng())
                });


                mapBounds.extend(marker.position);

                google.maps.event.addListener(marker, 'mouseover', function () {
                    if (!clicked) {
                        infowindow.setContent(self.jntInfoWindowContent(recordDetails));
                        infowindow.open(map, this);
                    }
                });

                google.maps.event.addListener(marker, 'mouseout', function () {
                    if (!clicked) {
                        infowindow.close();
                    }
                });

                google.maps.event.addListener(marker, 'click', function () {
                    clicked = true;
                    infowindow.setContent(self.jntInfoWindowContent(recordDetails));
                    infowindow.open(map, this);
                });

                google.maps.event.addListener(infowindow, 'closeclick', function () {
                    clicked = false;
                });




                self.jntHideLoader();
              //  $('#myModal').modal('show');
                map.fitBounds(mapBounds);
              //  return marker;


            },

            jntInfoWindowContent: function(recordDetails){

                return '<div class="markerInfoWrapper"><ul><li> AdId: '+ recordDetails.id+'</li><li> AdName: '+ recordDetails.AdName+'</li><li> AdPrice: '+ recordDetails.AdPrice+'</li><li> AdRooms: '+ recordDetails.AdRooms+'</li><li> AdSquaremeters: '+ recordDetails.AdSquaremeters+'</li><li> AdPricePerSquaremeters: '+ recordDetails.AdPricePerSquaremeters+'</li><li> AdPostcode: '+ recordDetails.AdPostCode+'</li><li> AdCity: '+ recordDetails.AdCity+'</li><li> AdDistrict: '+ recordDetails.AdDistrict+'</li><li> AdExportTimeStamp: '+ recordDetails.ExportTimeStamp+'</li><li>AdUrl: <a target="_blank" href="https://' + recordDetails.AdUrl + '">'+ recordDetails.AdUrl+'</a></li></ul></div>';
            },

            dynamicSortingOptions: function () {

                $(document).on('click', '.add', function () {
                    var sorting_html = $('.clone-select tr').clone();
                    $('#item_table').append(sorting_html);
                });

                $(document).on('click', '.remove', function () {
                    $(this).closest('tr').remove();
                });

                $('.show-vb').click(function () { //you can give id or class name here for $('button')

                    $(this).text(function (i, old) {
                        old = old.trim();
                        return old == 'Show Visual Builder' ? 'Close Visual Builder' : 'Show Visual Builder';
                    });
                });

            },

            functionSortingOptions: function () {

                $(document).on('click', '.f-add', function () {
                    var functionAgg = $('.clone-agg-func tr').clone();
                    $('#func_table').append(functionAgg);
                });

                $(document).on('click', '.f-remove', function () {
                    $(this).closest('tr').remove();
                });
            },

            getTableAttributes: function () {

                $('select.getTables').on('change', function (e) {
                    self.jntShowLoader();
                    $('.show-on').removeClass('hide');

                    var optionSelected = $("option:selected", this);
                    var valueSelected = this.value;
                    var url = $(this).attr('data-href');

                    $.ajax({
                        url: url,
                        type: "post",
                        data: {
                            "_token": $('#csrf-token').attr('content'),
                            "table": valueSelected
                        },

                        success: function (data){
                            self.jntHideLoader();
                            //$('#builder').child().remove();
                            self.queryBuilder(data.filtersData);
                            $('.tablefields').find('option').remove();
                            $(data.schema).each(function (i, e) {
                                $('.tablefields').append($("<option></option>").attr("value", e).text(e));
                            });
                        },
                        error: function () {

                        }
                    });
                });
            },

            runQuery: function () {

                $('.click-button').on('click', function () {

                    var error = false;

                    $('#builder').on('validationError.queryBuilder', function () {
                        error = true;
                        //  $('.fa-spin').addClass('hide');
                        //   $('.spin-text').css('padding-left', '0');
                        //   $(document).find('.total-records').addClass('hide');
                    });

                    if (!error) {

                        self.jntShowLoader();
                        if ($.fn.DataTable.isDataTable('#ajax-table')) {
                            $('#ajax-table').DataTable().destroy();
                        }

                        //console.log($('#ajax-table'));

                        $('#ajax-table').find('thead tr').empty();
                        $('#ajax-table').find('tbody').empty();
                        //console.log($('#ajax-table'));

                        $('.fa-spin').removeClass('hide');
                        $('.spin-text').css('padding-left', '5px');


                        var sql_stmt = $('#builder').queryBuilder('getSQL', 'named');
                        var url = $(this).attr('data-href');
                        //console.log(sql_stmt);

                        var fields = '*';
                        $('.select-fields .select2').find(':selected').each(function (i, e) {

                            if (i == 0) {
                                fields = $(this).val();
                            } else {
                                fields = fields + ', ' + $(this).val();
                            }

                        });

                        var table = $('.getTables').find(':selected').val();

                        var sorting = '';
                        $("#item_table .tablefields").find(':selected').each(function (i, e) {

                            var method = $('#item_table .sort-method:eq(' + i + ')').find(':selected').val();

                            if (i == 0) {
                                sorting = $(this).val() + ' ' + method;
                            } else {
                                sorting = sorting + ', ' + $(this).val() + ' ' + method;
                            }

                        });

                        var agg_func = '';
                        $("#func_table .tablefields").find(':selected').each(function (i, e) {

                            var funName = $('#func_table .functions:eq(' + i + ')').find(':selected').val();
                            var asCoName = $('#func_table .as-text:eq(' + i + ')').val();
                            asCoName = asCoName.length > 0 ? ' AS ' + asCoName.replace(/\s/g, '_') : '';
                            if (i == 0) {
                                agg_func = funName + '(' + $(this).val() + ')' + asCoName;
                            } else {
                                agg_func = agg_func + ', ' + funName + '(' + $(this).val() + ')' + asCoName;
                            }

                        });

                        var group_by = '';
                        $('.group-by .select2').find(':selected').each(function (i, e) {


                            if (i == 0) {
                                group_by = $(this).val();
                            } else {
                                group_by = group_by + ', ' + $(this).val();
                            }

                        });

                        var limit = $('.query-limit').find(':selected').val();

                        $.ajax({
                            url: url,
                            type: "post",
                            data: {
                                "_token": $('#csrf-token').attr('content'),
                                "sql_obj": sql_stmt,
                                'fields': fields,
                                'table': table,
                                'sorting': sorting,
                                'limit': limit,
                                'group_by': group_by,
                                'agg_func': agg_func
                            },
                            success: function (data) {

                                self.jntHideLoader();
                                self.ajaxTable(data);

                                $('.fa-spin').addClass('hide');
                                $('.spin-text').css('padding-left', '0');
                                $('.show-vb').removeClass('hide');
                                // $('#collapseExample').removeClass('show');
                                $('.show-vb').trigger('click');

                                $("#ajax-table").DataTable({
                                    "order": [],
                                    "autoWidth": false,
                                    "aLengthMenu": [[25, 50, 500, 1000, -1], [25, 50, 500, 1000, "All"]],
                                    "iDisplayLength": 500,
                                    dom: 'lBfrtip',
                                    buttons: [
                                        {
                                            extend: 'csv',
                                            text: 'Export CSV',
                                            charset: 'utf-8',
                                            extension: '.csv',
                                            fieldSeparator: ';',
                                            fieldBoundary: '',
                                            filename: 'export',
                                            bom: true
                                        },
                                        {
                                            extend: 'excel',
                                            text: 'Export Excel',
                                            charset: 'utf-8',
                                            extension: '.xlsx',
                                            fieldSeparator: ';',
                                            fieldBoundary: '',
                                            filename: 'export',
                                            bom: true
                                        }
                                    ]
                                });

                            },
                            error: function () {

                            }
                        });
                    }
                });
            },

            ajaxTable: function (results) {

                if (results.length > 0) {

                    //$('.total-records').removeClass('hide').html('<strong>Total Records:</strong> ' + results.length);

                    $.each(results[0], function (i, e) {
                        $('#ajax-table').find('thead tr').append('<th>' + i + '</th>');
                    });

                    $(results).each(function (i, e) {
                        $('#ajax-table').find('tbody').append('<tr></tr>');
                        $.each(e, function (k, d) {
                            $('#ajax-table').find('tbody tr:eq(' + i + ')').append('<td>' + d + '</td>');
                        });
                    });

                    if ($('#ajax-table').find('thead tr').length > 1) {
                        $('#ajax-table').find('thead tr:not(:last)').remove();
                    }

                } else {
                    //$('.no-record').removeClass('hide');
                }

            },

            queryBuilder: function (filterAtr) {

                var rules_basic = {
                    condition: 'AND',
                    rules: [{
                        id: 'price',
                        operator: 'less',
                        value: 10.25
                    }, {
                        condition: 'OR',
                        rules: [{
                            id: 'category',
                            operator: 'equal',
                            value: 2
                        }, {
                            id: 'category',
                            operator: 'equal',
                            value: 1
                        }]
                    }]
                };


                $('#builder').queryBuilder({
                    plugins: [
                        // 'bt-tooltip-errors',
                        'not-group'
                    ],

                    filters: filterAtr
                });

            },

            editFormPopUp: function () {

                $('.update-filter').on('click', function () {

                    var old_value = $(this).attr("data-old");
                    var new_value = $(this).attr("data-new");
                    var id = $(this).attr("data-id");

                    $('.fil-old').val(old_value);
                    $('.fil-new').val(new_value);
                    $('.fil-id').val(id);

                });
            },

            gridFormSubmit: function (el) {
                $(el).closest('form').submit();
            },

            recordsPerPage: function () {
                $('.grids-control-records-per-page').on('change', function (e) {
                    self.gridFormSubmit($(this));
                });
            },

            inputMasking: function () {
                $('.usPhoneFax').inputmask({
                    "mask": "(999) 999-9999"
                })
            },

            jntShowLoader: function () {
              $(document).find('.jntLoader').show();
            },

            jntHideLoader: function () {
                $(document).find('.jntLoader').hide();
            },

            dateTimePicker: function () {
                $.datetimepicker.setLocale('en');
                $('.datetimepicker').datetimepicker({
                    format: 'Y/m/d H:i',
                    step: 15
                });
            },

            initializeDropZone: function (args) {

                //if(!$(args.selector).is('*')) return false;

                try {
                    //  console.log(args.selector);

                    $('input[type="submit"]').hide();

                    Dropzone.autoDiscover = false;
                    var uploadDesign = args.hiddenField;
                    var myDropzone = new Dropzone(args.selector, {
                        url: args.url,
                        init: function () {
                            this.on('success', function (file, responseText) {
                                var response = JSON.parse(responseText);
                                var fileuploded = file.previewElement.querySelector("[data-dz-name]");
                                fileuploded.innerHTML = response.message;
                                /*console.log('server');
                                 console.log(fileuploded.innerHTML);*/
                                //file.name =
                            });
                        },
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },

                        //paramName: "plan_doc",
                        paramName: args.uploadDocType,
                        //acceptedFiles: ".pdf",
                        acceptedFiles: args.acceptedFiles,
                        previewTemplate: $('#preview-template').html(),
                        thumbnailHeight: 120,
                        thumbnailWidth: 120,
                        maxFiles: (args.selector == '#dropzoneGrabDocs') ? 1 : 500,
                        maxFilesize: 20,
                        addRemoveLinks: true,
                        dictRemoveFile: 'Remove',
                        dictDefaultMessage: '<span class="bigger-150 bolder"><i class="ace-icon fa fa-caret-right red"></i> Drop files</span> to upload \ <span class="smaller-80 grey">(or click)</span> <br /> \ <i class="upload-icon ace-icon fa fa-cloud-upload blue fa-3x"></i>',
                        thumbnail: function (file, dataUrl) {
                            // console.log(file);
                            if (file.previewElement) {
                                $(file.previewElement).removeClass("dz-file-preview");
                                var images = $(file.previewElement).find("[data-dz-thumbnail]").each(function () {
                                    var thumbnailElement = this;
                                    thumbnailElement.alt = file.id;
                                    thumbnailElement.src = dataUrl;
                                });
                                setTimeout(function () {
                                    $(file.previewElement).addClass("dz-image-preview");
                                }, 1);
                            }
                        }

                    });

                    myDropzone.on("queuecomplete", function (multiple, i) {
                        $('input[type="submit"]').show();
                        var designUpload = [];
                        jQuery(myDropzone.files).each(function (e, i) {
                            var updatedName = i.previewElement.querySelector("[data-dz-name]").innerHTML;
                            //i.previewElement.id = i.name;
                            i.previewElement.id = updatedName;
                            //if (i.accepted) designUpload.push(i.name);
                            if (i.accepted) designUpload.push(updatedName);
                        });
                        uploadDesign.val(designUpload);

                        if (args.reload) window.location.reload();
                    });

                    myDropzone.on('removedfile', function (file) {
                        var valArr = uploadDesign.val().split(',');
                        var index = valArr.indexOf(file.previewElement.id);
                        if (index > -1) valArr.splice(index, 1);
                        uploadDesign.val(valArr);
                    });

                    //  console.log(myDropzone);

                } catch (e) {
                    console.log(e);
                    //alert('Dropzone.js does not support older browsers!');
                }
            },

            randString: function (length) {
                var result = '';
                var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
                var charactersLength = characters.length;
                for (var i = 0; i < length; i++) {
                    result += characters.charAt(Math.floor(Math.random() * charactersLength));
                }
                return result;
            },

            submitData: function (args) {

                var url = $(args).attr('action');
                var method = 'POST';

                $.ajax({
                    url: url,
                    type: method,
                    data: $(args).serialize(),
                    success: function (data) {
                    },
                    error: function () {

                    }
                });
            }
        };

    $(document).ready(function () {
        APIFY.init();
    });

})(window, document, jQuery);
